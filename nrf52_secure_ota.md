# NRF52 Secure OTA

## 1. Generate keys

1. Generate a private key
    ```sh
    nrfutil keys generate priv.pem
    ```
    > The private key is used for generating the DFU zip package.

2. Generate a public key
    ```sh
    nrfutil keys display --key pk --format code priv.pem --out_file dfu_public_key.c
    ```
    > The public key is used by the bootloader.

## 2. Build the secure bootloader

1. Use one of the example secure dfu bootloader and build it.
    
    Examples of secure bootloader can be found in the SDK:
    `<SDK_ROOT>\examples\dfu\secure_bootloader\`

2. Save the generated hex file as we will merge it with the initial firmware.

## 3. Create initial firmware with OTA bootloader

1. Generate bootloader settings page.
    ```sh
    nrfutil settings generate --family NRF52840 \
    --application <app_hex> \
    --application-version <app_ver> \
    --bootloader-version <bl_ver> \
    --bl-settings-version 2 \
    --app-boot-validation VALIDATE_GENERATED_CRC \
    --sd-boot-validation VALIDATE_GENERATED_CRC \
    --softdevice <sd.hex> \
    --key-file <private.pem> \
    <bl_settings.hex>
    ```

2. Merge bootloader with bootloader settings page.
    ```sh
    mergehex --merge <bl.hex> <bl_settings.hex> -o <blws.hex>
    ```

3. Merge softdevice with application.
    ```sh
    mergehex --merge <sd.hex> <app_v1.hex> -o <sd_app_v1.hex>
    ```

4. Merge files generated at #2 and #3.
    ```sh
    mergehex --merge <sd_app_v1.hex> <blws.hex> -o <merged_v1.hex>
    ```

> **Note:** If you don't use Segger Embeded Studio for building bootloader then you may have to merge the Master Boot Record file with the bootloader otherwise the device might not boot correctly.

> MBR filepath: `<SDK_ROOT>\components\softdevice\mbr\hex\mbr_nrf52_2.4.1_mbr.hex`

## 4. Flash the initial firmware

1. Fully erase the device.
    ```sh
    nrfjprog --family NRF52 --eraseall
    ```

2. Flash the firmware.
    ```sh
    nrfjprog --family NRF52 --program <merged_v1.hex> --reset
    ```

## 5. Generate the DFU update package

To perform OTA update of the appliation firmware we have to create a zip package containing the application firmware file.

1. Update your application, and build it. Save the hex file for generating the zip package.

2. Generate the DFU zip package.
    ```sh
    nrfutil pkg generate \
    --application <app.hex> \
    --application-version <app_version> \
    --app-boot-validation VALIDATE_GENERATED_CRC \
    --key-file <private_key> \
    --hw-version 52 \
    --sd-req <SD_Firmware_ID> \
    <out.zip>
    ```

    > The `<SD_Firmware_ID>` can be found from the softdevice relase notes. Look for "The Firmware ID of this SoftDevice" line in the release notes file.
    
    > Release notes filepath:
    `nRF5_SDK_17.0.2_d674dde\components\softdevice\s140\doc\s140_nrf52_7.2.0_release-notes.pdf`

## 6. Perform OTA update

* For BLE OTA, use the nrf connect app.

* For serial OTA, use the nrfutil tool.
    ```sh
    nrfutil dfu serial -pkg <app_dfu.zip> -p <SERIAL_PORT> -b 115200 -fc 0
    ```

---

## Notes:

```sh
FLASH_PH_START=0x0
FLASH_PH_SIZE=0x100000
RAM_PH_START=0x20000000
RAM_PH_SIZE=0x40000
FLASH_START=0xe4000
FLASH_SIZE=0x1a000
RAM_START=0x20000008
RAM_SIZE=0x3fff8
```