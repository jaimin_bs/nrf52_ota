Copy the projects in the following locations after you have installed the **nRF5_SDK_17.0.2_d674dde** SDK.

1. Copy `ble_app_blinky_serialota` project to `nRF5_SDK_17.0.2_d674dde\examples\ble_peripheral` directory.

2. Copy `bootloader\pca10056_uart_debug` project to `nRF5_SDK_17.0.2_d674dde\examples\dfu\secure_bootloader` directory.

**To build bootloader:**

Follow the full guide in the `nrf52_secure_ota.md` file.