
#include <string.h>
#include "esp_log.h"

#include "pc_uart.h"

#define TAG "PC_UART"

uint8_t pc_rbuffer[PC_BUF_SIZE] = {0};

static uint32_t get_millis()
{
    return esp_timer_get_time() / 1000;
}

static int PCSerial_send(uint8_t *data, uint16_t len)
{
    return uart_write_bytes(UART_PC_PORT, (const char *) data, len);
}

static bool PCSerial_read(uint8_t *data_out, uint16_t *len_out, uint32_t timeout_ms)
{
    bool status = false;
    uint16_t rbuffer_len = 0;
    uint32_t read_timeout = get_millis() + timeout_ms;

    // try reading data is available or until timeout
    while (rbuffer_len == 0)
    {
        rbuffer_len = uart_read_bytes(UART_PC_PORT, pc_rbuffer, PC_BUF_SIZE, pdMS_TO_TICKS(PC_RX_TIMEOUT_MS));

        // format check
        if ( (rbuffer_len > 4) && (pc_rbuffer[0] == 0xEE) && (pc_rbuffer[rbuffer_len-1] == 0xEF) )
        {
            *len_out = rbuffer_len-4;
            memcpy(data_out, &pc_rbuffer[3], rbuffer_len-4);

            status = true;
            break;
        }

        if (get_millis() > read_timeout)
        {
            break;
        }
    }

    return status;
}



bool PCSerial_init()
{
    esp_err_t err;

    uart_config_t uart_pc_config = DEFAULT_PC_UART_CONFIG;

    err = uart_driver_install(UART_PC_PORT, PC_BUF_SIZE, PC_BUF_SIZE, 0, NULL, 0);
    
    if (err == ESP_OK)
    {
        err = uart_param_config(UART_PC_PORT, &uart_pc_config);
    }

    if (err == ESP_OK)
    {
        err = uart_set_pin(UART_PC_PORT, UART_PC_TXD, UART_PC_RXD, UART_PC_RTS, UART_PC_CTS);
    }

    ESP_ERROR_CHECK(err);

    return (err == ESP_OK);
}

bool PCSerial_getFileSize(uint8_t fileType, uint16_t *fileLen, uint32_t timeout_ms)
{
    bool status = false;
    uint8_t cmd[4] = {0xEE, 0x01, fileType, 0xEF}; 
    uint8_t response[2] = {0};
    uint16_t response_len = 0;
    uint32_t timeout = get_millis() + timeout_ms;
    
    *fileLen = 0;

    while ( get_millis() < timeout )
    {
        printf("[PC] Get filesize...\r\n");

        PCSerial_send(cmd, sizeof(cmd));

        // read response
        if ( PCSerial_read(response, &response_len, 1000) )
        {
            *fileLen = (response[1] << 8) | response[0];
            status = true;
            break;
        }

        // vTaskDelay(pdMS_TO_TICKS(100));
    }

    if (status == false)
    {
        printf("Error: Couldn't get length of init packet from PC.\r\n");
    }

    return status;
}

bool PCSerial_getPkt(uint8_t fileType, uint16_t length, uint16_t offset, uint8_t *response_out, uint16_t *response_len_out)
{
    bool status = false;
    uint32_t timeout = get_millis() + 4000;

    uint8_t cmd[8];
    
    cmd[0] = 0xEE;
    cmd[1] = 0x02; // req. type = 0x02
    cmd[2] = fileType;
    cmd[3] = length & 0xFF;
    cmd[4] = (length>>8) & 0xFF;
    cmd[5] = offset & 0xFF;
    cmd[6] = (offset>>8) & 0xFF;
    cmd[7] = 0xEF;

    while( get_millis() < timeout )
    {
        printf("[PC] Get packet, length: %d, offset: %d\r\n", length, offset);

        PCSerial_send(cmd, sizeof(cmd));

        // read response
        if ( PCSerial_read(response_out, response_len_out, 2000) )
        {
            ESP_LOG_BUFFER_HEX(TAG, response_out, *response_len_out);

            if (*response_len_out == length)
            {
                status = true;
                break;
            }
        }

        vTaskDelay(pdMS_TO_TICKS(100));
    }

    return status;
}

bool PCSerial_getCRC(uint8_t fileType, uint16_t length, uint16_t offset, uint32_t *crc_out)
{
    bool status = false;
    uint8_t response_out[4] = {0};
    uint16_t response_len_out;
    uint32_t timeout_ms = get_millis() + 2000;

    uint8_t cmd[8];
    
    cmd[0] = 0xEE;
    cmd[1] = 0x03;
    cmd[2] = fileType;
    cmd[3] = length & 0xFF;
    cmd[4] = (length>>8) & 0xFF;
    cmd[5] = offset & 0xFF;
    cmd[6] = (offset>>8) & 0xFF;
    cmd[7] = 0xEF;

    while (get_millis() < timeout_ms)
    {
        printf("[PC] Get CRC32...\r\n");

        PCSerial_send(cmd, sizeof(cmd));

        // read response
        if ( PCSerial_read(response_out, &response_len_out, 2000) )
        {
            ESP_LOG_BUFFER_HEX(TAG, response_out, response_len_out);

            *crc_out = (response_out[3] << 24) | \
                       (response_out[2] << 16) | \
                       (response_out[1] << 8) | \
                       response_out[0];

            status = true;
            break;
        }

        vTaskDelay(pdMS_TO_TICKS(100));
    }

    return status;
}
