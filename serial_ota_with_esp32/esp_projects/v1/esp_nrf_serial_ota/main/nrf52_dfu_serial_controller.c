#include "nrf52_dfu_serial_controller.h"
#include "nrf/slip.h"
#include <string.h>
#include "esp_log.h"

#include "pc_uart.h"

#define TAG "DFU_CONTROLLER"

uint8_t nrf_rbuffer[NRF_BUF_SIZE] = {0};
uint8_t nrf_wbuffer[NRF_BUF_SIZE] = {0};
uint8_t slip_buffer[NRF_BUF_SIZE] = {0};

uint8_t *p_slip_buffer;
uint16_t decoded_buffer_size = 0;

slip_t slipPkt;

static uint32_t get_millis()
{
    return esp_timer_get_time() / 1000;
}

static int dfu_send(uint8_t *data, uint16_t len)
{
    uint32_t cbuffer_len = 0;

    // send ping cmd
    slip_encode(nrf_wbuffer, data, len, &cbuffer_len);

    // ESP_LOG_BUFFER_HEX(TAG, nrf_wbuffer, cbuffer_len);

    return uart_write_bytes(UART_NRF_PORT, (const char *) nrf_wbuffer, cbuffer_len);
}

static uint8_t * dfu_read(uint16_t *len_out, uint32_t timeout_ms)
{
    ret_code_t slip_err;
    int rbuffer_len = 0;
    uint32_t read_timeout = get_millis() + timeout_ms;

    // try reading data is available or until timeout
    while (rbuffer_len == 0)
    {
        rbuffer_len = uart_read_bytes(UART_NRF_PORT, nrf_rbuffer, NRF_BUF_SIZE, pdMS_TO_TICKS(NRF_RX_TIMEOUT_MS));

        if (get_millis() > read_timeout)
        {
            break;
        }
    }

    // decode data
    if (rbuffer_len > 0)
    {
        slip_err = slip_decode(&slipPkt, nrf_rbuffer, rbuffer_len);
        
        if (slip_err == NRF_SUCCESS)
        {
            *len_out = slipPkt.current_index;
            
            return slipPkt.p_buffer;
        }
        else
        {
            printf("Failed to decode data. SLIP Error code: 0x%X\r\n", slip_err);
            ESP_LOG_BUFFER_HEX(TAG, slipPkt.p_buffer, rbuffer_len);
        }
    }

    return NULL;
}

static uint32_t crc32_compute(uint8_t const * p_data, uint32_t size, uint32_t const * p_crc)
{
    uint32_t crc;

    crc = (p_crc == NULL) ? 0xFFFFFFFF : ~(*p_crc);
    for (uint32_t i = 0; i < size; i++)
    {
        crc = crc ^ p_data[i];
        for (uint32_t j = 8; j > 0; j--)
        {
            crc = (crc >> 1) ^ (0xEDB88320U & ((crc & 1) ? 0xFFFFFFFF : 0));
        }
    }
    return ~crc;
}

static bool create_obj_request(uint8_t obj_type, uint32_t obj_size, uint32_t timeout_ms)
{
    bool status = false;
    uint8_t req_cmd[6] = {0};
    uint32_t req_timeout = get_millis() + timeout_ms;

    req_cmd[0] = 0x01;
    req_cmd[1] = obj_type;
    req_cmd[2] = obj_size & 0xFF; // LSB
    req_cmd[3] = (obj_size >> 8) & 0xFF;
    req_cmd[4] = (obj_size >> 16) & 0xFF;
    req_cmd[5] = (obj_size >> 24) & 0xFF; // MSB

    while ( get_millis() < req_timeout )
    {
        printf("> Create new object, type: %d\r\n", req_cmd[1]);

        dfu_send(req_cmd, sizeof(req_cmd));

        // read response
        p_slip_buffer = dfu_read(&decoded_buffer_size, 1000);

        if (decoded_buffer_size > 0)
        {
            ESP_LOG_BUFFER_HEX(TAG, p_slip_buffer, decoded_buffer_size);

            if ( (p_slip_buffer[0] == 0x60) && \
                 (p_slip_buffer[1] == req_cmd[0]) && \
                 (p_slip_buffer[2] == 0x01) )
            {
                status = true;
                printf("create obj request accepted.\r\n");
                break;
            }
        }

        vTaskDelay(pdMS_TO_TICKS(100));
    }

    if (status == false)
    {
        printf("Error: DFU target did not respond for create object request.\r\n");
    }

    return status;
}

static bool select_obj_request(uint8_t obj_type, uint32_t *max_size, uint32_t *cur_offset, uint32_t *cur_crc32, uint32_t timeout_ms)
{
    bool status = false;
    uint8_t obj_sel_cmd[2];

    uint32_t cmd_timeout = get_millis() + timeout_ms;

    obj_sel_cmd[0] = 0x06; // obj sel cmd
    obj_sel_cmd[1] = obj_type;

    while ( get_millis() < cmd_timeout )
    {
        printf("> Select object, type: %d\r\n", obj_sel_cmd[1]);

        dfu_send(obj_sel_cmd, sizeof(obj_sel_cmd));

        // read response
        p_slip_buffer = dfu_read(&decoded_buffer_size, 1000);

        if (decoded_buffer_size > 0)
        {
            if ( (p_slip_buffer[0] == 0x60) && \
                 (p_slip_buffer[1] == obj_sel_cmd[0]) && \
                 (p_slip_buffer[2] == 0x01) )
            {
                ESP_LOG_BUFFER_HEX(TAG, p_slip_buffer, decoded_buffer_size);

                *max_size = (p_slip_buffer[6] << 24) | \
                            (p_slip_buffer[5] << 16) | \
                            (p_slip_buffer[4] << 8)  | \
                            p_slip_buffer[3]; // LSB
                
                *cur_offset = (p_slip_buffer[10] << 24) | \
                              (p_slip_buffer[9] << 16) | \
                              (p_slip_buffer[8] << 8)  | \
                              p_slip_buffer[7]; // LSB
                
                *cur_crc32 = (p_slip_buffer[14] << 24) | \
                             (p_slip_buffer[13] << 16) | \
                             (p_slip_buffer[12] << 8)  | \
                             p_slip_buffer[11]; // LSB

                printf("Max Size: %u, Offset: %u, CRC32: %u\r\n", *max_size, *cur_offset, *cur_crc32);
                
                status = true;
                break;
            }
        }

        vTaskDelay(pdMS_TO_TICKS(100));
    }

    if (status == false)
    {
        printf("Error: DFU target did not respond for select object request.\r\n");
    }

    return status;
}

static uint32_t send_crc32_request(uint32_t timeout_ms, uint32_t *len_out)
{
    uint8_t cmd = 0x03;
    uint32_t nrf_crc32 = 0;
    uint32_t timeout = get_millis() + timeout_ms;

    dfu_send(&cmd, sizeof(cmd));

    // read response
    while (get_millis() < timeout)
    {
        printf("Get CRC32...\r\n");

        // read response
        p_slip_buffer = dfu_read(&decoded_buffer_size, 1000);

        if (decoded_buffer_size > 0)
        {
            ESP_LOG_BUFFER_HEX(TAG, p_slip_buffer, decoded_buffer_size);

            if ( (p_slip_buffer[0] == 0x60) && \
                 (p_slip_buffer[1] == 0x03) && \
                 (p_slip_buffer[2] == 0x01) )
            {
                *len_out = (p_slip_buffer[6] << 24) | \
                           (p_slip_buffer[5] << 16) | \
                           (p_slip_buffer[4] << 8)  | \
                           p_slip_buffer[3]; // LSB
                
                nrf_crc32 = (p_slip_buffer[10] << 24) | \
                            (p_slip_buffer[9] << 16) | \
                            (p_slip_buffer[8] << 8)  | \
                            p_slip_buffer[7]; // LSB

                break;
            }
        }
    }

    return nrf_crc32;
}

static bool send_execobj_request(uint32_t timeout_ms)
{
    bool status = false;
    uint8_t req_cmd[1] = {0x04};
    uint32_t timeout = get_millis() + timeout_ms;

    while(get_millis() < timeout)
    {
        printf("> Execute\r\n");

        dfu_send(req_cmd, sizeof(req_cmd));

        // read response
        p_slip_buffer = dfu_read(&decoded_buffer_size, 1000);

        if (decoded_buffer_size > 0)
        {
            ESP_LOG_BUFFER_HEX(TAG, p_slip_buffer, decoded_buffer_size);

            if ( (p_slip_buffer[0] == 0x60) && \
                 (p_slip_buffer[1] == req_cmd[0]) )
            {
                if (p_slip_buffer[2] == 0x01)
                {
                    status = true;
                }
                else
                {
                    printf("Error occurred after execute request.\r\n");
                }

                break;
            }

            #if 0
            if ( (p_slip_buffer[0] == 0x60) && \
                 (p_slip_buffer[1] == req_cmd[0]) && \
                 (p_slip_buffer[2] == 0x01) )
            {
                status = true;
                break;
            }
            else
            {
                status = false;
                break;
            }
            #endif
        }
    }

    return status;
}

static bool send_file_as_objs(uint8_t fileType, uint16_t fileSize, uint8_t objType, uint16_t mtu)
{
    bool status = false;
    bool sendobj = false;
    bool objValid = false;
    bool newobj = false;

    uint32_t max_size = 0;
    uint32_t cur_offset = 0;
    uint32_t cur_crc32 = 0;

    uint16_t bytes_remaining = 0;
    uint16_t objSize = 0;

    uint16_t req_len = 0;
    uint16_t req_offset = 0;
    uint16_t last_req_offset = 0;

    uint16_t bytes_sent = 0;

    uint8_t *pkt = NULL;
    uint8_t *p_buffer = NULL;
    uint16_t buffer_len = 0;

    uint16_t mtu_size = mtu-1;

    uint32_t crc32 = 0;
    uint32_t last_crc32 = 0;
    uint32_t nrf_crc32 = 0;
    uint32_t pc_crc32 = 0;
    uint32_t nrfReceived = 0;

    // select obj type
    if ( select_obj_request(objType, &max_size, &cur_offset, &cur_crc32, 1000) )
    {
        // create new obj
        if ( (cur_offset == 0) && (cur_crc32 == 0) )
        {
            newobj = true;
            bytes_remaining = fileSize;
        }

        // file already sent, so send execute request
        else if (cur_offset == fileSize)
        {
            // verify if the file is same
            if ( PCSerial_getCRC(fileType, fileSize, 0, &pc_crc32) && \
                 (cur_crc32 == pc_crc32) )
            {
                objValid = true; // skip to #4

                bytes_remaining = 0;
                bytes_sent = fileSize;

                printf("File was already sent.\r\n");
            }
            else
            {
                printf("Error: different file was sent earlier, cannot continue.\r\n");
                return status;
            }
        }

        // file partially sent
        else
        {
            if (cur_offset % max_size == 0)
            {
                // last obj was sent completely, create new obj to continue
                if ( PCSerial_getCRC(fileType, cur_offset, 0, &pc_crc32) && \
                    (cur_crc32 == pc_crc32) )
                {               
                    newobj = true;

                    bytes_sent = cur_offset;
                    crc32 = cur_crc32;
                    req_offset = cur_offset;

                    bytes_remaining = fileSize - bytes_sent;

                    last_crc32 = crc32;
                    last_req_offset = req_offset;

                    printf("Continuing interrupted transfer...\r\nOffset: %u, CRC32: %u\r\n", cur_offset, cur_crc32);
                }
            }
            else // last obj was sent interrupted, continue
            {
                sendobj = true;

                bytes_sent = cur_offset;
                crc32 = cur_crc32;
                req_offset = cur_offset;

                bytes_remaining = fileSize - bytes_sent;

                last_crc32 = crc32;
                last_req_offset = req_offset;

                printf("Continuing interrupted obj transfer...\r\nOffset: %u, CRC32: %u\r\n", cur_offset, cur_crc32);
            }
            
            // else
            // {
            //     printf("Error: CRC32 does not match, cannot continue.\r\n");
            //     return status;
            // }
        }
    }

    // create pkt buffer for sending obj as packets
    if (bytes_remaining > 0)
    {
        pkt = (uint8_t *) malloc(sizeof(uint8_t) * mtu);

        if (pkt != NULL)
        {   
            p_buffer = pkt + 1; // buffer to read from PC
        }
        else
        {
            printf("Error: could not allocate memory.\r\n");
            return status;
        }
    }

    // send file as objs
    do
    {
        // 1. create obj
        if ( newobj )
        {
            objSize = bytes_remaining > max_size ? max_size : bytes_remaining;

            sendobj = create_obj_request(objType, objSize, 1000);

            if (sendobj)
            {
                // do not create new obj unless current obj is sent & is valid
                newobj = false;
            }
        }

        // 2. write obj, packetizing obj based on mtu size limit
        if ( sendobj )
        {
            req_len = bytes_remaining > mtu_size ? mtu_size : bytes_remaining;

            if ( PCSerial_getPkt(fileType, req_len, req_offset, p_buffer, &buffer_len) )
            {
                if ( buffer_len == req_len )
                {
                    pkt[0] = 0x08; // obj write cmd

                    // send to nrf
                    dfu_send(pkt, req_len+1);

                    ESP_LOG_BUFFER_HEX(TAG, p_buffer, buffer_len);

                    if (crc32 == 0)
                    {
                        crc32 = crc32_compute(p_buffer, buffer_len, NULL);    
                    }
                    else
                    {
                        crc32 = crc32_compute(p_buffer, buffer_len, &crc32);
                    }

                    memset(pkt, 0, mtu);

                    req_offset += req_len; // 0+64=64,64+64=128,128+13=141
                    bytes_sent = req_offset;

                    bytes_remaining = fileSize - bytes_sent; // 141-64=77,141-128=13,141-141=0

                    printf("[PC] bytes remaining: %d\r\n", bytes_remaining);
                }
                else // resend PCSerial_getPkt req
                {
                    continue;
                }
            }
            else // resend PCSerial_getPkt req
            {
                continue;
            }

            // 3. validate last sent obj
            if ( (bytes_sent == fileSize) /* file already sent, also when fileSize < max_size*/ || \
                 ((bytes_sent > 0) && (bytes_sent % max_size == 0)) )
            {
                nrfReceived = 0;
                nrf_crc32 = send_crc32_request(2000, &nrfReceived);

                printf("NRFCRC32: %u, PKTCRC32: %u\r\nTotal Received: %u bytes\r\n", nrf_crc32, crc32, nrfReceived);

                if (crc32 == nrf_crc32)
                {
                    last_crc32 = crc32;
                    last_req_offset = req_offset;

                    objValid = true;
                }
                else // resend last obj
                {
                    req_offset = last_req_offset;
                    bytes_sent = req_offset;
                    bytes_remaining = fileSize - bytes_sent;
                    crc32 = last_crc32;

                    printf("Resending obj from with offset: %u\r\n", req_offset);
                    break; // testing
                }
            }
        }

        // 4. execute obj
        if ( objValid )
        {
            objValid = false;

            send_execobj_request(2000);

            // obj is sent succesfful, send remaining objs
            newobj = true;
        }

    } while (bytes_remaining != 0);

    if ( (bytes_sent == fileSize) && (bytes_remaining == 0) )
    {
        printf("File was sent successfully\r\n");
        status = true;
    }

    if (pkt != NULL)
    {
        free(pkt);
    }

    return status;
}

static bool send_file(uint8_t fileType, uint16_t mtu)
{
    bool status = false;

    uint16_t fileSize = 0;
    uint8_t objType = 0xFF;

    printf("> Sending %s file\r\n", fileType == 0x01 ? "InitPkt" : (fileType == 0x02 ? "Firmware" : "unknown"));

    // get file size
    if ( PCSerial_getFileSize(fileType, &fileSize, 4000) )
    {
        printf("fileSize: %d\r\n", fileSize);
    }
    else
    {
        return status;
    }

    // define object type
    if (fileType == 0x01) // init packet file
    {
        objType = 0x01; // command obj
    }
    else if (fileType == 0x02) // firmware file
    {
        objType = 0x02; // data obj
    }

    status = send_file_as_objs(fileType, fileSize, objType, mtu);

    return status;
}



bool DFUSerial_init()
{
    esp_err_t err;

    // UART port of nRF52
    uart_config_t uart_nrf_config = DEFAULT_NRF_UART_CONFIG;

    err = uart_driver_install(UART_NRF_PORT, NRF_BUF_SIZE, NRF_BUF_SIZE, 0, NULL, 0);
    
    if (err == ESP_OK)
    {
        err = uart_param_config(UART_NRF_PORT, &uart_nrf_config);
    }

    if (err == ESP_OK)
    {
        err = uart_set_pin(UART_NRF_PORT, UART_NRF_TXD, UART_NRF_RXD, UART_NRF_RTS, UART_NRF_CTS);
    }

    slipPkt.p_buffer = slip_buffer;
    slipPkt.current_index = 0;
    slipPkt.buffer_len = NRF_BUF_SIZE;
    slipPkt.state = SLIP_STATE_DECODING;

    ESP_ERROR_CHECK(err);

    // PC UART
    PCSerial_init();

    return (err == ESP_OK);
}

bool DFUSerial_ping(uint32_t timeout_ms)
{
    uint8_t ping_counter = 1;
    uint8_t ping_cmd[2] = {0};

    bool ping_response = false;

    uint32_t ping_timeout = get_millis() + timeout_ms;

    while ( get_millis() < ping_timeout )
    {
        printf("> Sending ping request...\r\n");

        ping_cmd[0] = 0x09; // ping cmd
        ping_cmd[1] = ping_counter; // ping counter

        // send ping cmd
        dfu_send(ping_cmd, sizeof(ping_cmd));
        
        // read ping response
        decoded_buffer_size = 0;
        p_slip_buffer = dfu_read(&decoded_buffer_size, 1000);

        if (decoded_buffer_size > 0)
        {
            ESP_LOG_BUFFER_HEX(TAG, p_slip_buffer, decoded_buffer_size);

            if ( (p_slip_buffer[0] == 0x60) &&
                 (p_slip_buffer[1] == ping_cmd[0]) &&
                 (p_slip_buffer[2] == 0x01) &&
                 (p_slip_buffer[3] == ping_counter) )
            {
                ping_response = true;
                break;
            }
        }

        ping_counter++;
        vTaskDelay(pdMS_TO_TICKS(100));
    }

    if (ping_response == false)
    {
        printf("Error: DFU target did respond to ping request.\r\n");
    }

    return ping_response;
}

bool DFUSerial_setPRN(uint16_t prn, uint32_t timeout_ms)
{
    bool status = false;

    uint8_t prn_cmd[3];

    uint32_t prn_timeout = get_millis() + timeout_ms;

    prn_cmd[0] = 0x02;              // prn cmd
    prn_cmd[1] = prn & 0xFF;        // prn value LSB
    prn_cmd[2] = (prn >> 8) & 0xFF; // prn value MSB

    while ( get_millis() < prn_timeout )
    {
        printf("> Set PRN...\r\n");

        // set prn
        dfu_send(prn_cmd, sizeof(prn_cmd));

        // read response
        p_slip_buffer = dfu_read(&decoded_buffer_size, 1000);

        if (decoded_buffer_size > 0)
        {
            ESP_LOG_BUFFER_HEX(TAG, p_slip_buffer, decoded_buffer_size);

            if ( (p_slip_buffer[0] == 0x60) && \
                 (p_slip_buffer[1] == prn_cmd[0]) && \
                 (p_slip_buffer[2] == 0x01) )
            {
                status = true;
                break;
            }
        }

        vTaskDelay(pdMS_TO_TICKS(100));
    }

    if (status == false)
    {
        printf("Error: DFU target did not respond with PRN acknowledegment.\r\n");
    }

    return status;
}

bool DFUSerial_getMTU(uint16_t *p_mtu, uint32_t timeout_ms)
{
    bool status = false;

    uint16_t mtu = 0;
    uint8_t mtu_cmd = 0x07;

    uint32_t mtu_timeout = get_millis() + timeout_ms;

    while ( get_millis() < mtu_timeout )
    {
        printf("> Get MTU...\r\n");

        // get mtu
        dfu_send(&mtu_cmd, sizeof(mtu_cmd));

        // read response
        p_slip_buffer = dfu_read(&decoded_buffer_size, 1000);

        if (decoded_buffer_size > 0)
        {
            ESP_LOG_BUFFER_HEX(TAG, p_slip_buffer, decoded_buffer_size);

            if ( (p_slip_buffer[0] == 0x60) && \
                 (p_slip_buffer[1] == mtu_cmd) && \
                 (p_slip_buffer[2] == 0x01) )
            {
                mtu = 0;

                mtu |= p_slip_buffer[3];      // LSB
                mtu |= p_slip_buffer[4] << 8; // MSB

                *p_mtu = mtu/2;

                status = true;
                break;    
            }
        }

        vTaskDelay(pdMS_TO_TICKS(100));
    }

    if (status == false)
    {
        printf("Error: DFU target did not respond with required MTU size.\r\n");
    }

    return status;
}

bool DFUSerial_sendInitPkt(uint16_t mtu)
{
    return send_file(0x01, mtu);
}

bool DFUSerial_sendFirmware(uint16_t mtu)
{
    return send_file(0x02, mtu);
}
