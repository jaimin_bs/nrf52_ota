#ifndef PC_UART_H
#define PC_UART_H

#include "driver/uart.h"
#include "driver/gpio.h"

#define DEFAULT_PC_UART_CONFIG {            \
    .baud_rate = 115200,                    \
    .data_bits = UART_DATA_8_BITS,          \
    .parity    = UART_PARITY_DISABLE,       \
    .stop_bits = UART_STOP_BITS_1,          \
    .flow_ctrl = UART_HW_FLOWCTRL_DISABLE,  \
    .source_clk = UART_SCLK_APB,            \
}

#define UART_PC_PORT    UART_NUM_1
#define UART_PC_TXD     GPIO_NUM_4
#define UART_PC_RXD     GPIO_NUM_5

#define UART_PC_RTS  (UART_PIN_NO_CHANGE)
#define UART_PC_CTS  (UART_PIN_NO_CHANGE)

#define PC_BUF_SIZE (256)
#define PC_RX_TIMEOUT_MS 10

bool PCSerial_init();
bool PCSerial_getFileSize(uint8_t fileType, uint16_t *fileLen, uint32_t timeout_ms);
bool PCSerial_getPkt(uint8_t fileType, uint16_t length, uint16_t offset, uint8_t *response_out, uint16_t *response_len_out);
bool PCSerial_getCRC(uint8_t fileType, uint16_t length, uint16_t offset, uint32_t *crc_out);

#endif /* PC_UART_H */