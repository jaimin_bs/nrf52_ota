/*
    1. ESP32 is the DFU Controller.
    2. nRF52 is the DFU Target.
    3. ESP32 requests PC with offset and packet length and PC responds
       with packets of firmware package.
*/

#include <stdio.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include <string.h>

#include "nrf52_dfu_serial_controller.h"
#include "nrf/slip.h"

#include "pc_uart.h"

static const char *TAG = "main";

static bool run_task = false;

static uint16_t offset = 0;
static uint16_t length = 64;

#if 0
static void echo_task(void *arg)
{
    while (1) 
    {
        // uart_write_bytes(UART_PC_PORT, (const char *) pc_wbuffer, 3);
        // int len = uart_read_bytes(UART_PC_PORT, pc_rbuffer, 64, 20 / portTICK_RATE_MS);

        // Write data to NRF
        if (len > 0)
        {
            // end of init pkt from PC
            if ( (len == 3) && 
                (pc_rbuffer[0] == 0x00) && 
                (pc_rbuffer[1] == 0xEE) && (pc_rbuffer[2] == 0xEE) )
            {
                printf("End of init packet\r\n");
            }

            // end of firmware from PC
            else if ( (len == 3) && 
                (pc_rbuffer[0] == 0x01) && 
                (pc_rbuffer[1] == 0xEE) && (pc_rbuffer[2] == 0xEE) )
            {
                printf("End of firmware packet\r\n");
            }
            
            // forward to NRF but encode it using SLIP protocol
            else if (len > 0)
            {
                DFUSerial_send(pc_rbuffer, len);
                offset += length;
            }
        }

        vTaskDelay(pdMS_TO_TICKS(15000));
    }
}
#endif

void app_main(void)
{
    bool status = false;

    uint16_t targetMTU = 0;

    uint16_t len_out;

    // NRF UART
    DFUSerial_init();

    // 1. ping, detect NRF in DFU mode
    status = DFUSerial_ping(5000);

    if (status)
    {
        // 2. set PRN
        status = DFUSerial_setPRN(0x00, 5000);

        // get MTU
        if (status)
        {
            status = DFUSerial_getMTU(&targetMTU, 5000);

            if ( targetMTU > 0 )
                printf("MTU Size: %d\r\n", targetMTU);
        }
    }

    if (status)
    {
        // 3. send Init packet
        status = DFUSerial_sendInitPkt(targetMTU);
    }

    if (status)
    {
        // 4. send firmware
        status = DFUSerial_sendFirmware(targetMTU);
    }

    // xTaskCreate(echo_task, "uart_echo_task", 1024, NULL, 10, NULL);

    length = 64;
    offset = 0;

    while(1)
    {
        run_task = true;
        // pc_wbuffer[0] = 1;  // type
        // pc_wbuffer[1] = length; // length
        // pc_wbuffer[2] = offset;  // offset

        vTaskDelay(pdMS_TO_TICKS(5000));
    }
}
