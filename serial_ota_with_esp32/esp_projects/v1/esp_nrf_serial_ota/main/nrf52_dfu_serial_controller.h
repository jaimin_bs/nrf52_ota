#ifndef NRF52_DFU_SERIAL_CONTROLLER_H
#define NRF52_DFU_SERIAL_CONTROLLER_H

#include "driver/uart.h"
#include "driver/gpio.h"

#define DEFAULT_NRF_UART_CONFIG {           \
    .baud_rate = 115200,                    \
    .data_bits = UART_DATA_8_BITS,          \
    .parity    = UART_PARITY_DISABLE,       \
    .stop_bits = UART_STOP_BITS_1,          \
    .flow_ctrl = UART_HW_FLOWCTRL_DISABLE,  \
    .source_clk = UART_SCLK_APB,            \
}

#define UART_NRF_PORT   UART_NUM_2
#define UART_NRF_TXD    GPIO_NUM_17
#define UART_NRF_RXD    GPIO_NUM_16

#define UART_NRF_RTS  (UART_PIN_NO_CHANGE)
#define UART_NRF_CTS  (UART_PIN_NO_CHANGE)

#define NRF_BUF_SIZE (256)
#define NRF_RX_TIMEOUT_MS 100

bool DFUSerial_init();
bool DFUSerial_ping(uint32_t timeout);
bool DFUSerial_setPRN(uint16_t prn, uint32_t timeout_ms);
bool DFUSerial_getMTU(uint16_t *p_mtu, uint32_t timeout_ms);
bool DFUSerial_sendInitPkt(uint16_t mtu);
bool DFUSerial_sendFirmware(uint16_t mtu);

#endif /* NRF52_DFU_SERIAL_CONTROLLER_H */