# ESP32-NRF52 Serial OTA

ESP32 acts as a DFU controller and NRF52 acts as DFU target.

Connection:

[PC]<->[ESP32]<->[NRF52]

## ESP32

The DFU Controller is also connected to the PC to get the firmware files as packets. For this you should run the `esp_serial_test.py` python script. After running the script, DFU controller will request firmware packets length of packet and file offset. On receiving this packet it is relayed to the DFU target device.

> **ESP-IDF 4.2** was used.

**ESP32 pins for PC UART:**

    RX: GPIO_NUM_5
    TX: GPIO_NUM_4

**ESP32 pins for NRF UART:**

    RX: RX2, GPIO_NUM_16
    TX: TX2, GPIO_NUM_17

## NRF52

The NRF52 device should be flashed with the initial version (`.\app_v1\mergedv1.hex`) of the application binary.

**NRF UART pins for serial OTA (ESP32):**

    RX: P1.13, NRF_GPIO_PIN_MAP(1,13)
    TX: P1.14, NRF_GPIO_PIN_MAP(1,14)
        
**NRF UART for RTT log viewer:**

    RX: 8 (P0.8), RX_PIN_NUMBER
    TX: 6 (P0.6), TX_PIN_NUMBER

> Refer pca10056.h file for NRF side pins

## UART Settings

This is the common UART settings used by all of the UART ports. Including ESP32, NRF52 and PC.

```sh
Baudrate: 115200
8 data bits
parity disabled
1 stop bit
HW flow control disabled
```

---

## How to use it

1. Download this folder.

2. NRF52

    a. Connect the NRF52 board with JLINK debug port to flash the initial version of binaries.
    
    ```sh
    nrfjprog --family NRF52 --eraseall

    nrfjprog --family NRF52 --program ".\app_v1\mergedv1.hex" --reset
    ```
    
    b. Put the NRF in bootloader mode. Hold switch 4 while resetting the device.

3. ESP32

    a. Build the `esp_nrf_serial_ota` project.

    ```sh
    idf.py build
    ```

    b. Flash the binaries.

    ```sh
    idf.py -p <SERIAL_PORT> flash monitor
    ```

4. PC

    a. Run the python `esp_serial_test.py` script

    ```sh
    esp_serial_test.py -p <SERIAL_PORT> -z app_v2.zip
    ```

The DFU process should start and the NRF52 should have been updated with app_v2 binary.