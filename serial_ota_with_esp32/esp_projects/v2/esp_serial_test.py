'''
    UART Settings:
        Baudrate: 115200
        8 data bits
        parity disabled
        1 stop bit
        HW flow control disabled

    ESP32 pins for PC UART:
        RX: GPIO_NUM_5
        TX: GPIO_NUM_4
    
    ESP32 pins for NRF UART:
        RX: RX2, GPIO_NUM_16
        TX: TX2, GPIO_NUM_17
    
    Refer pca10056.h file for NRF side pins:

        NRF UART pins for serial OTA (ESP32):
            RX: P1.13, NRF_GPIO_PIN_MAP(1,13)
            TX: P1.14, NRF_GPIO_PIN_MAP(1,14)
        
        NRF UART for RTT log viewer:
            RX: 8 (P0.8), RX_PIN_NUMBER
            TX: 6 (P0.6), TX_PIN_NUMBER
'''

import os
import sys
import getopt
import serial
from zipfile import ZipFile
import json
import time
import ctypes

init_buffer = bytes()
firmware_buffer = bytes()

# print usage text
def print_usage():
    print("\nUsage:\n")
    print("  ", os.path.basename(sys.argv[0]), '-p <serial_port> -z <zip_file>')

# Parse arguments
try:
    opts, args = getopt.getopt(sys.argv[1:],"p:z:")
except getopt.GetoptError:
    print_usage()
    sys.exit(2)

if (opts == []) or (len(opts) != 2):
    print_usage()
    sys.exit(2)
else:
    opts = dict(opts)

# input arguments
com_port = opts["-p"]
dfu_zip = opts["-z"]

# initialize serial port
try:
    esp_serial = serial.Serial(com_port, 115200, timeout=0.1)
except serial.serialutil.SerialException as err:
    print("\nSerial Error:", err)
    sys.exit(2)

def extract_zip_file(zip_archive):
    if os.path.isfile(zip_archive):
        try:
            with ZipFile(zip_archive, "r") as zfp:
                zfp.extractall()
                zfp.close()
                
            return True
        except Exception as err:
            print("Zip Error:", err)
    
    else:
        print("\nFile Error: '" + zip_archive + "' file doesn't exist")
    
    return False

def process_manifest_file(manifest_file):
    """
    Process the manifest json file from the nrf dfu package (zip) file.
    This version handles application update only.
    """
    with open(manifest_file, "r") as mf:
        json_data = json.loads(mf.read())
        mfjson = json_data["manifest"]
        mf.close()

        # application update
        application_json = mfjson["application"]

        return application_json

def read_file(target_file):
    with open(target_file, "rb") as target_fp:
        data = target_fp.read()
        target_fp.close()

        return data

def process_serial_data():
    """
        ESP32 requests for a packet of desired length with required
        offset from the start of file. There are two types of
        requests, init packet and firmware packet requests.

        ESP32 request format.

            Byte - Description:
            ----   ------------ 

            [1] - Request type
                    0x01 - get length of file
                    0x02 - get pkt of given length and file offset
                    0x03 - get crc32 for a given length and file offset
                    
            [2] - File type
                    0x01 - Init pkt file
                    0x02 - Firmware file

            Parameters:
            
            [3...4] -  uint16_t length
            [5...8] -  uint32_t offset

        Response format:
        
            [0...n] - Response data 

        Example:
        1. init pkt file length request:
                [0x01, 0x01], len = 2
           response:
                [LENGTH[0], LENGTH[1], LENGTH[2], LENGTH[3]], len = 4

        2. init pkt request with length & offset paramters
                [0x02, 0x01, LENGTH[0], LENGTH[1], OFFSET[0], OFFSET[1], OFFSET[2], OFFSET[3]], len = 8
           response:
                [...bytes of data...]

        3. get crc32 request
                [0x03, 0x01, LENGTH[0], LENGTH[1], OFFSET[0], OFFSET[1], OFFSET[2], OFFSET[3]], len = 8
            response:
                [CRC32[0], CRC32[1], CRC32[2], CRC32[3]], len = 4
    """

    bytes_read = esp_serial.read(8)

    bytes_read_len = len(bytes_read)

    if (bytes_read_len >= 2):

        print("<--", bytes_read)

        req_type = bytes_read[0]
        file_type = bytes_read[1]

        # process req 0x01, get length of file
        if (bytes_read_len == 2):

            # Filesize request
            if (req_type == 0x01):

                # init pkt req.
                if file_type == 0x01:
                    init_buffer_len = len(init_buffer)
                    data = bytes( [(init_buffer_len & 0xff), ((init_buffer_len>>8) & 0xff), ((init_buffer_len>>16) & 0xff), ((init_buffer_len>>24) & 0xff)] )

                # firm. pkt req.
                elif file_type == 0x02:
                    firm_buffer_len = len(firmware_buffer)
                    data = bytes( [(firm_buffer_len & 0xff), ((firm_buffer_len>>8) & 0xff), ((firm_buffer_len>>16) & 0xff), ((firm_buffer_len>>24) & 0xff)] )
                
                esp_serial.write(data)
                print("-->", list(data))

            else:
                print("Error: Invalid request, expected 0x01")

        elif (bytes_read_len == 8):

            length = ((bytes_read[3] << 8) & 0xFF00) | (bytes_read[2] & 0x00FF)
            offset = ((bytes_read[7] << 24) & 0xFF00) | ((bytes_read[6] << 16) & 0xFF00) | ((bytes_read[5] << 8) & 0xFF00) | (bytes_read[4] & 0x00FF)
            
            # file as packets
            if (req_type == 0x02):
                
                # init pkt req.
                if (file_type == 0x01):
                    print("[init_pkt] offset:", offset, "length:", length)
                    data = init_buffer[offset : offset+length]

                # firm. pkt req.
                elif file_type == 0x02:
                    print("[firmware] offset:", offset, "length:", length)
                    data = firmware_buffer[offset : offset+length]

                esp_serial.write(data)
                print("-->", list(data))

            # CRC32 request
            elif (req_type == 0x03):

                # init pkt req.
                if file_type == 0x01:
                    crc32 = crc32_compute(init_buffer[offset : offset+length], length)
                    data = bytes( [(crc32 & 0xff), ((crc32>>8) & 0xff), ((crc32>>16) & 0xff), ((crc32>>24) & 0xff)] )
                
                # firm. pkt req.
                elif file_type == 0x02:
                    crc32 = crc32_compute(firmware_buffer[offset : offset+length], length)
                    data = bytes( [(crc32 & 0xff), ((crc32>>8) & 0xff), ((crc32>>16) & 0xff), ((crc32>>24) & 0xff)] )
                
                print("CRC32:", ctypes.c_uint32(crc32))
                esp_serial.write(data)
                print("-->", list(data))

            else:
                print("Error: Invalid request, expected 0x02 or 0x03")
        
        # else:
        #     print("Error: Unexpected packet format.")

def crc32_compute(data, len, old_crc=None):
    if old_crc == None:
        crc = 0xFFFFFFFF
    else:
        crc = ~old_crc
    
    for byte in data:
        crc = crc ^ byte
        for i in range(0,8):
            if (crc & 1):
                temp_1 = 0xFFFFFFFF
            else:
                temp_1 = 0
            temp_2 = 0xEDB88320 & temp_1
            crc = (crc >> 1) ^ temp_2
    
    return ~crc

def main():
    global init_buffer
    global firmware_buffer

    # extract the zip pkg
    extracted = extract_zip_file(dfu_zip)

    if extracted:
        # parse the json
        app_json = process_manifest_file("manifest.json")

        if app_json:        
            # initialize filenames
            app_bin_file = app_json["bin_file"]
            init_pkt_file = app_json["dat_file"]

            if os.path.isfile(init_pkt_file) and os.path.isfile(app_bin_file):
                # read files
                init_buffer = read_file(init_pkt_file)
                firmware_buffer = read_file(app_bin_file)

                # run
                while(1):
                    # rxd_bytes = serial_loop(2)
                    process_serial_data()
            
            else:
                print("\nError: couldn't find application binaries in your zip file")
                return 1
        
        else:
            print("\nError: couldn't find 'application' key-value pair from the manifest.json file, zip archive is not valid. Please regenerate the application update package using nrfutil.exe tool and retry.")
            return 1

main()

# def crc32_test(data, len, old_crc=None):
#     if old_crc == None:
#         crc = 0xFFFFFFFF
#     else:
#         crc = ~old_crc
    
#     for byte in data:
#         crc = crc ^ byte
#         for i in range(0,8):
#             if (crc & 1):
#                 temp_1 = 0xFFFFFFFF
#             else:
#                 temp_1 = 0
#             temp_2 = 0xEDB88320 & temp_1
#             crc = (crc >> 1) ^ temp_2
    
#     return ~crc;

# data1 = [0xDE, 0xAD, 0xBE, 0xEF]
# data2 = [0x32, 0x33, 0x34, 0x35]
# data3 = [0x56, 0x57, 0x58, 0x59]
# data = data1 + data2 + data3 + [64]
# crc1 = crc32_test(data1, len(data1))
# crc2 = crc32_test(data2, len(data2), crc1)
# crc3 = crc32_test(data3, len(data3), crc2)
# crc4 = crc32_test(data, len(data))
# print(crc3)
# print(crc4)
