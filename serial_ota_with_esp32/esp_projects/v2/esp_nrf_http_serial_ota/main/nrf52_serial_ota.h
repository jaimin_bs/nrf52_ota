#ifndef NRF52_SERIAL_OTA_H
#define NRF52_SERIAL_OTA_H

typedef enum {
    OTA_STATE_IDLE,

    OTA_STATE_DFU_INIT,
    OTA_STATE_DFU_CONNECT,

    OTA_STATE_DFU_SEND_INIT_FILE,
    OTA_STATE_DFU_SEND_FIRMWARE_FILE,
    OTA_STATE_DFU_IN_PROGRESS,
    OTA_STATE_DFU_FILE_SENT,

    OTA_STATE_END
} otaState_et;

typedef enum {
    OTA_HOST_HTTP,
    OTA_HOST_PCSERIAL,
    OTA_HOST_INVALID
} otaHost_et;

void ota_sync();
void ota_start(otaHost_et host);

#endif /* NRF52_SERIAL_OTA_H */
