#include "esp_log.h"
#include "esp_tls.h"
#include "esp_http_client.h"

#include "ota_http_client.h"

#define TAG "OTA_HTTP_CLIENT"

static esp_http_client_handle_t httpClient;
static esp_http_client_config_t httpConfig;

static fileDownload_st file = DEFAULT_FILEDOWNLOAD_VALUE;

static otaBuffer_st *pOtaBuffer = NULL;
static bool isConnected = false;

static httpState_et httpState = HTTP_CLIENT_IDLE;

extern const char howsmyssl_com_root_cert_pem_start[] asm("_binary_howsmyssl_com_root_cert_pem_start");
extern const char howsmyssl_com_root_cert_pem_end[]   asm("_binary_howsmyssl_com_root_cert_pem_end");

static bool http_init()
{
    httpClient = esp_http_client_init(&httpConfig);

    if ( httpClient == NULL )
    {
        printf("Error: Failed to initialize http client\r\n");
        return false;
    }

    return true;
}

static bool http_connect()
{
    esp_err_t err = esp_http_client_open(httpClient, 0);

    if ( err != ESP_OK )
    {
        printf("Error: http client failed to connect\r\n");
        return false;
    }

    isConnected = true;

    return true;
}

static httpState_et http_download()
{
    static int fileSize = 0;
    int bytesRead = 0;
    uint32_t bytesReqd = 0;

    httpState_et nextState = HTTP_CLIENT_END;

    // get file size
    if ( file.downloadStarted == false )
    {
        fileSize = esp_http_client_fetch_headers(httpClient);
    
        if ( (fileSize < 0) || (fileSize == 0) )
        {
            printf("Error: Invalid filesize.\r\n");
            return nextState; // HTTP_CLIENT_END
        }

        // initialize to track download progress
        file.size = fileSize;
        file.offset = 0;
        file.bytesRemaining = file.size;
        file.downloadStarted = true;

        pOtaBuffer->dataReceivedLen = 0;
        pOtaBuffer->eof = false;

        printf("Filesize: %d\r\n", file.size);
    }

    // download file
    if ( file.downloadStarted )
    {
        bytesReqd = (file.bytesRemaining > pOtaBuffer->size) ? \
                    pOtaBuffer->size : \
                    file.bytesRemaining;
        
        bytesRead = esp_http_client_read(httpClient, 
                                        (char *) pOtaBuffer->data,
                                        bytesReqd);

        // read successful
        if ( bytesRead == bytesReqd )
        {
            file.offset = file.offset + bytesRead;
            file.bytesRemaining = file.size - file.offset;

            printf("requested: %d, received: %d, remaining: %d\r\n",\
                    bytesReqd, bytesRead, file.bytesRemaining);

            // will cause ota_run_dfu() to relay it to DFU target
            pOtaBuffer->dataReceivedLen = bytesRead;

            // end of file reached, download complete
            if (file.bytesRemaining == 0)
            {
                pOtaBuffer->eof = true;
                file.downloadStarted = false;

                nextState = HTTP_CLIENT_END;
                return nextState;
            }

            nextState = HTTP_CLIENT_DOWNLOAD;
            return nextState;
        }
    }

    return nextState; // HTTP_CLIENT_END;
}

static void http_cleanup()
{
    // cleanup, else causes incorrect behavior in state machine
    file.size = 0;
    file.offset = 0;
    file.bytesRemaining = 0;
    file.downloadStarted = false;

    pOtaBuffer->dataReceivedLen = 0;
    pOtaBuffer->eof = false;
    
    if (isConnected)
    {
        isConnected = false;
        esp_http_client_close(httpClient);
    }

    if (httpClient != NULL)
    {
        esp_http_client_cleanup(httpClient);
    }
}

void http_ota_sync()
{
    // all of the http state tranisitions for file download
    // are handled here itself
    switch (httpState) 
    {
        // do nothing
        case HTTP_CLIENT_IDLE:
            break;
        
        // initialize required resources
        case HTTP_CLIENT_INIT:
            printf("HTTPSTATE: INIT\r\n");
            httpState = http_init() ? HTTP_CLIENT_CONNECT : HTTP_CLIENT_END;
            break;
        
        // establish connection
        case HTTP_CLIENT_CONNECT:
        printf("HTTPSTATE: CONNECT\r\n");
            httpState = http_connect() ? HTTP_CLIENT_DOWNLOAD : HTTP_CLIENT_END;
            break;
        
        // download file in packets
        case HTTP_CLIENT_DOWNLOAD:
            printf("HTTPSTATE: DOWNLOADING\r\n");
            httpState = http_download(); // returns HTTP_CLIENT_DOWNLOAD or
                                         // HTTP_CLIENT_END
            break;
        
        // cleanup http resources
        case HTTP_CLIENT_END:
            printf("HTTPSTATE: END\r\n");
            http_cleanup();
            httpState = HTTP_CLIENT_IDLE;
            break;
    }
}

void http_ota_init(const char *url, otaBuffer_st *buffer)
{
    httpConfig.url = url;

    pOtaBuffer = buffer;
}

void http_ota_start()
{
    httpState = HTTP_CLIENT_INIT;
}
