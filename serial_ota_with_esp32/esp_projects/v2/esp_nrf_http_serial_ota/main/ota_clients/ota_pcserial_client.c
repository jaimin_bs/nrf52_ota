#include <string.h>

#include "esp_log.h"

#include "ota_pcserial_client.h"

#define TAG "OTA_PCSERIAL_CLIENT"

static pcSerialState_et pcSerialState = PCSERIAL_CLIENT_IDLE;
static nrf_dfu_obj_type_t targetObjType = NRF_DFU_OBJ_TYPE_INVALID;
static otaBuffer_st *pOtaBuffer = NULL;
static fileDownload_st file = DEFAULT_FILEDOWNLOAD_VALUE;

static uint32_t get_millis()
{
    return esp_timer_get_time() / 1000;
}

static int pcserial_send(uint8_t *data, uint16_t len)
{
    return uart_write_bytes(UART_PC_PORT, (const char *) data, len);
}

static bool pcserial_read(uint8_t *data_out, uint32_t reqdLen, uint32_t *len_out, uint32_t timeout_ms)
{
    bool status = false;
    uint32_t rbuffer_len = 0;
    uint32_t read_timeout = get_millis() + timeout_ms;

    // try reading data is available or until timeout
    while (rbuffer_len == 0)
    {
        rbuffer_len = uart_read_bytes(UART_PC_PORT, data_out, reqdLen, pdMS_TO_TICKS(PC_RX_TIMEOUT_MS));

        if (rbuffer_len > 0)
        {
            *len_out = rbuffer_len;
            status = true;
            break;
        }

        if (get_millis() > read_timeout)
        {
            break;
        }
    }

    return status;
}

static uint32_t pcserial_get_filesize()
{
    uint32_t fileSize = 0;

    uint8_t cmd[2] = {0x01, targetObjType}; 
    uint8_t response[4] = {0};
    uint32_t response_len = 0;

    printf("[PC] Get filesize...\r\n");

    pcserial_send(cmd, sizeof(cmd));

    // read response
    if ( pcserial_read(response, 4, &response_len, PC_CMD_RES_TIMEOUT_MS) )
    {
        fileSize = (response[3] << 24) | \
                    (response[2] << 16) | \
                    (response[1] << 8) | \
                    response[0]; 
    }

    return fileSize;
}

static uint32_t pcserial_file_read(uint8_t *buffer, uint16_t reqdBytes)
{
    uint32_t bytesRcvd = 0;
    uint8_t cmd[8] = {0};
    uint8_t response[4] = {0};

    cmd[0] = 0x02; // req. type = 0x02 // file download in packets
    cmd[1] = targetObjType;
    cmd[2] = reqdBytes & 0xFF;  // uin16_t length
    cmd[3] = (reqdBytes>>8) & 0xFF;
    cmd[4] = file.offset & 0xFF; // uint32_t offset
    cmd[5] = (file.offset>>8) & 0xFF;
    cmd[6] = (file.offset>>16) & 0xFF;
    cmd[7] = (file.offset>>24) & 0xFF;

    if ( pcserial_send(cmd, sizeof(cmd)) )
    {
        pcserial_read(buffer, reqdBytes, &bytesRcvd, PC_CMD_RES_TIMEOUT_MS*10);
    }

    return bytesRcvd;
}

static pcSerialState_et pcserial_download()
{
    pcSerialState_et nextState = PCSERIAL_CLIENT_END;
    uint32_t bytesReqd = 0;
    uint32_t bytesRead = 0;

    // get filesize
    if (file.downloadStarted == false)
    {
        file.size = pcserial_get_filesize();

        printf("Filesize: %d\r\n", file.size);

        if ( (file.size > 0) == false )
        {
            printf("Error: Invalid filesize.\r\n");
            return nextState; // PCSERIAL_CLIENT_END
        }

        // initialize to track download progress
        file.offset = 0;
        file.bytesRemaining = file.size;
        file.downloadStarted = true;

        pOtaBuffer->dataReceivedLen = 0;
        pOtaBuffer->eof = false;
    }

    // download file
    if (file.downloadStarted)
    {
        bytesReqd = (file.bytesRemaining > pOtaBuffer->size) ? \
                    pOtaBuffer->size : \
                    file.bytesRemaining;
        
        bytesRead = pcserial_file_read(pOtaBuffer->data, bytesReqd);

        // read successful
        if ( bytesRead == bytesReqd )
        {
            file.offset = file.offset + bytesRead;
            file.bytesRemaining = file.size - file.offset;

            printf("requested: %d, received: %d, remaining: %d\r\n",\
                    bytesReqd, bytesRead, file.bytesRemaining);

            // will cause ota_run_dfu() to relay it to DFU target
            pOtaBuffer->dataReceivedLen = bytesRead;

            // end of file reached, download complete
            if (file.bytesRemaining == 0)
            {
                pOtaBuffer->eof = true;
                file.downloadStarted = false;

                nextState = PCSERIAL_CLIENT_END;
                return nextState;
            }

            nextState = PCSERIAL_CLIENT_DOWNLOAD;
            return nextState;
        }
    }

    return nextState;
}

static bool pcserial_init()
{
    esp_err_t err;

    uart_config_t uart_pc_config = DEFAULT_PC_UART_CONFIG;

    err = uart_driver_install(UART_PC_PORT, PC_BUF_SIZE, PC_BUF_SIZE, 0, NULL, 0);
    
    if (err == ESP_OK)
    {
        err = uart_param_config(UART_PC_PORT, &uart_pc_config);
    }

    if (err == ESP_OK)
    {
        err = uart_set_pin(UART_PC_PORT, UART_PC_TXD, UART_PC_RXD, UART_PC_RTS, UART_PC_CTS);
    }

    ESP_ERROR_CHECK(err);

    return (err == ESP_OK);
}

static bool pcserial_cleanup()
{
    esp_err_t err = uart_driver_delete(UART_PC_PORT);

    // cleanup, else causes incorrect behavior in state machine
    file.size = 0;
    file.offset = 0;
    file.bytesRemaining = 0;
    file.downloadStarted = false;

    pOtaBuffer->dataReceivedLen = 0;
    pOtaBuffer->eof = false;

    return (err == ESP_OK);
}

void pcserial_ota_sync()
{
    switch (pcSerialState)
    {
        // do nothing
        case PCSERIAL_CLIENT_IDLE:
            break;

        case PCSERIAL_CLIENT_INIT:
            printf("PCSERIAL State: INIT\r\n");
            pcSerialState = pcserial_init() ? \
                            PCSERIAL_CLIENT_DOWNLOAD : \
                            PCSERIAL_CLIENT_END;
            break;

        // download file in packets
        case PCSERIAL_CLIENT_DOWNLOAD:
            printf("PCSERIAL State: DOWNLOAD\r\n");
            pcSerialState = pcserial_download(); 
            // returns PCSERIAL_CLIENT_DOWNLOAD or
            // PCSERIAL_CLIENT_END
            break;

        // cleanup serial resources
        case PCSERIAL_CLIENT_END:
            printf("PCSERIAL State: END\r\n");
            pcserial_cleanup();
            pcSerialState = PCSERIAL_CLIENT_IDLE;
            break;
    }
}

void pcserial_ota_init(nrf_dfu_obj_type_t objType, otaBuffer_st *buffer)
{
    targetObjType = objType;
    pOtaBuffer = buffer;
}

void pcserial_ota_start()
{
    pcSerialState = PCSERIAL_CLIENT_INIT;
}