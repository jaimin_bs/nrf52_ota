#ifndef OTA_PCSERIAL_CLIENT_H
#define OTA_PCSERIAL_CLIENT_H

#include "nrf52_dfu_serial_controller.h"
#include "ota_client_buffer.h"

#include "driver/uart.h"
#include "driver/gpio.h"

#define DEFAULT_PC_UART_CONFIG {            \
    .baud_rate = 115200,                    \
    .data_bits = UART_DATA_8_BITS,          \
    .parity    = UART_PARITY_DISABLE,       \
    .stop_bits = UART_STOP_BITS_1,          \
    .flow_ctrl = UART_HW_FLOWCTRL_DISABLE,  \
    .source_clk = UART_SCLK_APB,            \
}

#define UART_PC_PORT    UART_NUM_1
#define UART_PC_TXD     GPIO_NUM_4
#define UART_PC_RXD     GPIO_NUM_5

#define UART_PC_RTS  (UART_PIN_NO_CHANGE)
#define UART_PC_CTS  (UART_PIN_NO_CHANGE)

#define PC_BUF_SIZE (256)

#define PC_RX_TIMEOUT_MS 50
#define PC_CMD_RES_TIMEOUT_MS (PC_RX_TIMEOUT_MS * 4)

typedef enum {
    PCSERIAL_CLIENT_IDLE,
    PCSERIAL_CLIENT_INIT,
    PCSERIAL_CLIENT_DOWNLOAD,
    PCSERIAL_CLIENT_END
} pcSerialState_et;

void pcserial_ota_sync();
void pcserial_ota_init(nrf_dfu_obj_type_t objType, otaBuffer_st *buffer);
void pcserial_ota_start();

#endif /* OTA_PCSERIAL_CLIENT_H */
