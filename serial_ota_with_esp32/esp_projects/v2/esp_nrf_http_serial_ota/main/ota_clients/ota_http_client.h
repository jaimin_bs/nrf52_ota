#ifndef OTA_HTTP_CLIENT_H
#define OTA_HTTP_CLIENT_H

#include "ota_client_buffer.h"

typedef enum {
    HTTP_CLIENT_IDLE,
    HTTP_CLIENT_INIT,
    HTTP_CLIENT_CONNECT,
    HTTP_CLIENT_DOWNLOAD,
    HTTP_CLIENT_END
} httpState_et;

void http_ota_init(const char *url, otaBuffer_st *buffer);
void http_ota_sync();
void http_ota_start();

#endif /* OTA_HTTP_CLIENT_H */