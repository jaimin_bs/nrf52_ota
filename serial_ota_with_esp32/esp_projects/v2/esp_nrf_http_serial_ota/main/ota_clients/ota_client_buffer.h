#ifndef OTA_CLIENT_BUFFER_H
#define OTA_CLIENT_BUFFER_H

#include <stdint.h>
#include <stdbool.h>

#define DEFAULT_FILEDOWNLOAD_VALUE { \
    .size = 0,                       \
    .offset = 0,                     \
    .bytesRemaining = 0,             \
    .downloadStarted = false         \
}

typedef struct {
    uint8_t *data;
    uint16_t size;
    uint16_t dataReceivedLen;
    bool eof;
} otaBuffer_st;

typedef struct {
    uint32_t size;
    uint32_t offset;
    uint32_t bytesRemaining;
    bool downloadStarted;
} fileDownload_st;

#endif /* OTA_CLIENT_BUFFER_H */