#include <string.h>

#include "esp_log.h"

#include "nrf52_dfu_serial_controller.h"
#include "slip.h"

#include "esp_tls.h"
#include "esp_http_client.h"
#include "esp_log.h"



#define TAG "NRF52_DFU_CONTROLLER"

#define NRF_TX_BUFFER_SIZE (MAX_BUF_SIZE+1)
#define NRF_RX_BUFFER_SIZE MAX_BUF_SIZE



uint8_t nrfTxBuffer[NRF_TX_BUFFER_SIZE] = {0};
uint8_t nrfRxBuffer[NRF_RX_BUFFER_SIZE] = {0};

slip_t slipPkt;
uint8_t slipBuffer[MAX_BUF_SIZE] = {0};
uint16_t slipBufferLen = 0; // used for read
uint8_t *pSlipPkt; // used for read

esp_http_client_handle_t httpClient;
esp_http_client_config_t httpConfig;

uint32_t file_crc32 = 0;



static uint32_t get_millis()
{
    return esp_timer_get_time() / 1000;
}

static int dfu_send(uint8_t *data, uint16_t len)
{
    uint32_t cbuffer_len = 0;

    slip_encode(nrfTxBuffer, data, len, &cbuffer_len);

    return uart_write_bytes(UART_NRF_PORT, (const char *) nrfTxBuffer, cbuffer_len);
}

static uint8_t * dfu_read(uint16_t *len_out, uint32_t timeout_ms)
{
    ret_code_t slip_err;
    int rbuffer_len = 0;
    uint32_t read_timeout = get_millis() + timeout_ms;

    // try reading until data is available or until timeout
    while (rbuffer_len == 0)
    {
        rbuffer_len = uart_read_bytes(UART_NRF_PORT, nrfRxBuffer, NRF_RX_BUFFER_SIZE, pdMS_TO_TICKS(NRF_RX_TIMEOUT_MS));

        if (get_millis() > read_timeout)
        {
            break;
        }
    }

    // decode data
    if (rbuffer_len > 0)
    {
        slip_err = slip_decode(&slipPkt, nrfRxBuffer, rbuffer_len);
        
        if (slip_err == NRF_SUCCESS)
        {
            *len_out = slipPkt.current_index;
            
            return slipPkt.p_buffer;
        }
        else
        {
            printf("Failed to decode data. SLIP Error code: 0x%X\r\n", slip_err);
            ESP_LOG_BUFFER_HEX(TAG, slipPkt.p_buffer, rbuffer_len);
        }
    }

    return NULL;
}

static uint32_t crc32_compute(uint8_t const * p_data, uint32_t size, uint32_t const * p_crc)
{
    uint32_t crc;

    crc = (p_crc == NULL) ? 0xFFFFFFFF : ~(*p_crc);
    for (uint32_t i = 0; i < size; i++)
    {
        crc = crc ^ p_data[i];
        for (uint32_t j = 8; j > 0; j--)
        {
            crc = (crc >> 1) ^ (0xEDB88320U & ((crc & 1) ? 0xFFFFFFFF : 0));
        }
    }
    return ~crc;
}

static bool create_obj_request(nrf_dfu_obj_type_t obj_type, uint32_t obj_size, uint32_t timeout_ms)
{
    bool status = false;
    uint8_t req_cmd[6] = {0};
    uint32_t req_timeout = get_millis() + timeout_ms;

    req_cmd[0] = NRF_DFU_OP_OBJECT_CREATE;
    req_cmd[1] = obj_type;
    req_cmd[2] = obj_size & 0xFF; // LSB
    req_cmd[3] = (obj_size >> 8) & 0xFF;
    req_cmd[4] = (obj_size >> 16) & 0xFF;
    req_cmd[5] = (obj_size >> 24) & 0xFF; // MSB

    while ( get_millis() < req_timeout )
    {
        printf("> Create new object, type: %d\r\n", req_cmd[1]);

        dfu_send(req_cmd, sizeof(req_cmd));

        // read response
        slipBufferLen = 0;
        pSlipPkt = dfu_read(&slipBufferLen, 1000);

        if (slipBufferLen > 0)
        {
            ESP_LOG_BUFFER_HEX(TAG, pSlipPkt, slipBufferLen);

            if ( (pSlipPkt[0] == NRF_DFU_OP_RESPONSE) && \
                 (pSlipPkt[1] == req_cmd[0]) && \
                 (pSlipPkt[2] == NRF_DFU_RES_CODE_SUCCESS) )
            {
                status = true;
                printf("create obj request accepted.\r\n");
                break;
            }
        }
    }

    if (status == false)
    {
        printf("Error: DFU target did not respond for create object request.\r\n");
    }

    return status;
}

static bool select_obj_request(nrf_dfu_obj_type_t obj_type, uint32_t *max_size, uint32_t *cur_offset, uint32_t *cur_crc32, uint32_t timeout_ms)
{
    bool status = false;
    uint8_t obj_sel_cmd[2];

    uint32_t cmd_timeout = get_millis() + timeout_ms;

    obj_sel_cmd[0] = NRF_DFU_OP_OBJECT_SELECT; // obj sel cmd
    obj_sel_cmd[1] = obj_type;

    while ( get_millis() < cmd_timeout )
    {
        printf("> Select object, type: %d\r\n", obj_sel_cmd[1]);

        dfu_send(obj_sel_cmd, sizeof(obj_sel_cmd));

        // read response
        slipBufferLen = 0;
        pSlipPkt = dfu_read(&slipBufferLen, DFU_RX_TIMEOUT_MS);

        if (slipBufferLen > 0)
        {
            if ( (pSlipPkt[0] == NRF_DFU_OP_RESPONSE) && \
                 (pSlipPkt[1] == obj_sel_cmd[0]) && \
                 (pSlipPkt[2] == NRF_DFU_RES_CODE_SUCCESS) )
            {
                ESP_LOG_BUFFER_HEX(TAG, pSlipPkt, slipBufferLen);

                *max_size = (pSlipPkt[6] << 24) | \
                            (pSlipPkt[5] << 16) | \
                            (pSlipPkt[4] << 8)  | \
                            pSlipPkt[3]; // LSB
                
                *cur_offset = (pSlipPkt[10] << 24) | \
                              (pSlipPkt[9] << 16) | \
                              (pSlipPkt[8] << 8)  | \
                              pSlipPkt[7]; // LSB
                
                *cur_crc32 = (pSlipPkt[14] << 24) | \
                             (pSlipPkt[13] << 16) | \
                             (pSlipPkt[12] << 8)  | \
                             pSlipPkt[11]; // LSB

                printf("Max Size: %u, Offset: %u, CRC32: %u\r\n", *max_size, *cur_offset, *cur_crc32);
                
                status = true;
                break;
            }
        }
    }

    if (status == false)
    {
        printf("Error: DFU target did not respond for select object request.\r\n");
    }

    return status;
}

static uint32_t send_crc32_request(uint32_t timeout_ms, uint32_t *len_out)
{
    uint8_t cmd[1] = {NRF_DFU_OP_CRC_GET};
    uint32_t nrf_crc32 = 0;
    uint32_t timeout = get_millis() + timeout_ms;

    dfu_send(cmd, sizeof(cmd));

    // read response
    while (get_millis() < timeout)
    {
        printf("Get CRC32...\r\n");

        // read response
        slipBufferLen = 0;
        pSlipPkt = dfu_read(&slipBufferLen, DFU_RX_TIMEOUT_MS);

        if (slipBufferLen > 0)
        {
            ESP_LOG_BUFFER_HEX(TAG, pSlipPkt, slipBufferLen);

            if ( (pSlipPkt[0] == NRF_DFU_OP_RESPONSE) && \
                 (pSlipPkt[1] == cmd[0]) && \
                 (pSlipPkt[2] == NRF_DFU_RES_CODE_SUCCESS) )
            {
                *len_out = (pSlipPkt[6] << 24) | \
                           (pSlipPkt[5] << 16) | \
                           (pSlipPkt[4] << 8)  | \
                           pSlipPkt[3]; // LSB
                
                nrf_crc32 = (pSlipPkt[10] << 24) | \
                            (pSlipPkt[9] << 16) | \
                            (pSlipPkt[8] << 8)  | \
                            pSlipPkt[7]; // LSB

                break;
            }
        }
    }

    return nrf_crc32;
}

static bool send_execobj_request(uint32_t timeout_ms)
{
    bool status = false;
    uint8_t req_cmd[1] = {NRF_DFU_OP_OBJECT_EXECUTE};
    uint32_t timeout = get_millis() + timeout_ms;

    while(get_millis() < timeout)
    {
        printf("> Execute\r\n");

        dfu_send(req_cmd, sizeof(req_cmd));

        // read response
        slipBufferLen = 0;
        pSlipPkt = dfu_read(&slipBufferLen, DFU_RX_TIMEOUT_MS);

        if (slipBufferLen > 0)
        {
            ESP_LOG_BUFFER_HEX(TAG, pSlipPkt, slipBufferLen);

            if ( (pSlipPkt[0] == NRF_DFU_OP_RESPONSE) && \
                 (pSlipPkt[1] == req_cmd[0]) )
            {
                if (pSlipPkt[2] == NRF_DFU_RES_CODE_SUCCESS)
                {
                    status = true;
                }
                else
                {
                    printf("Error: failed to execute obj.\r\nError code: %d\r\n", pSlipPkt[2]);
                }

                break;
            }
        }
    }

    return status;
}

static bool send_file_as_objs(uint16_t fileSize, nrf_dfu_obj_type_t objType, uint16_t mtu)
{
    bool status = false;
    bool sendobj = false;
    bool objValid = false;
    bool newobj = false;

    uint32_t max_size = 0;
    uint32_t cur_offset = 0;
    uint32_t cur_crc32 = 0;

    uint16_t bytes_remaining = 0;
    uint16_t objSize = 0;

    uint16_t req_len = 0;
    uint16_t req_offset = 0;
    uint16_t last_req_offset = 0;

    uint16_t bytes_sent = 0;

    uint8_t *pkt = NULL;
    uint8_t *p_buffer = NULL;
    uint16_t buffer_len = 0;

    uint16_t mtu_size = mtu-1;

    uint32_t crc32 = 0;
    uint32_t last_crc32 = 0;
    uint32_t nrf_crc32 = 0;
    uint32_t pc_crc32 = 0;
    uint32_t nrfReceived = 0;

    // select obj type
    if ( select_obj_request(objType, &max_size, &cur_offset, &cur_crc32, DFU_CMD_TIMEOUT_MS) )
    {
        // create new obj
        if ( (cur_offset == 0) && (cur_crc32 == 0) )
        {
            newobj = true;
            bytes_remaining = fileSize;
        }
        else
        {
            printf("Error: select obj response not handled.\r\n");
            return status;
        }

#if 0
        // file already sent, so send execute request
        else if (cur_offset == fileSize)
        {
            // verify if the file is same
            if ( PCSerial_getCRC(fileType, fileSize, 0, &pc_crc32) && \
                 (cur_crc32 == pc_crc32) )
            {
                objValid = true; // skip to #4

                bytes_remaining = 0;
                bytes_sent = fileSize;

                printf("File was already sent.\r\n");
            }
            else
            {
                printf("Error: different file was sent earlier, cannot continue.\r\n");
                return status;
            }
        }

        // file partially sent
        else
        {
            if (cur_offset % max_size == 0)
            {
                // last obj was sent completely, create new obj to continue
                if ( PCSerial_getCRC(fileType, cur_offset, 0, &pc_crc32) && \
                    (cur_crc32 == pc_crc32) )
                {               
                    newobj = true;

                    bytes_sent = cur_offset;
                    crc32 = cur_crc32;
                    req_offset = cur_offset;

                    bytes_remaining = fileSize - bytes_sent;

                    last_crc32 = crc32;
                    last_req_offset = req_offset;

                    printf("Continuing interrupted transfer...\r\nOffset: %u, CRC32: %u\r\n", cur_offset, cur_crc32);
                }
            }
            else // last obj was sent interrupted, continue
            {
                sendobj = true;

                bytes_sent = cur_offset;
                crc32 = cur_crc32;
                req_offset = cur_offset;

                bytes_remaining = fileSize - bytes_sent;

                last_crc32 = crc32;
                last_req_offset = req_offset;

                printf("Continuing interrupted obj transfer...\r\nOffset: %u, CRC32: %u\r\n", cur_offset, cur_crc32);
            }
            
            // else
            // {
            //     printf("Error: CRC32 does not match, cannot continue.\r\n");
            //     return status;
            // }
        }
#endif
    }

    // create pkt buffer for sending obj as packets
    if (bytes_remaining > 0)
    {
        pkt = (uint8_t *) malloc(sizeof(uint8_t) * mtu);

        if (pkt == NULL)
        {
            printf("Error: could not allocate memory.\r\n");
            return status;
        }

        p_buffer = pkt + 1; // buffer to read from PC
    }

    // send file as objs
    do {
        // 1. create obj
        if ( newobj )
        {
            objSize = bytes_remaining > max_size ? max_size : bytes_remaining;

            sendobj = create_obj_request(objType, objSize, DFU_CMD_TIMEOUT_MS);

            if (sendobj)
            {
                // do not create new obj unless current obj is sent & is valid
                newobj = false;
            }
        }

        // 2. write obj, packetizing obj based on mtu size limit
        if ( sendobj )
        {
            req_len = bytes_remaining > mtu_size ? mtu_size : bytes_remaining;

            buffer_len = esp_http_client_read(httpClient, (char *) p_buffer, req_len);

            if ( buffer_len == req_len )
            {
                pkt[0] = 0x08; // obj write cmd

                // send to nrf
                dfu_send(pkt, req_len+1);

                ESP_LOG_BUFFER_HEX(TAG, p_buffer, buffer_len);

                if (crc32 == 0)
                {
                    crc32 = crc32_compute(p_buffer, buffer_len, NULL);    
                }
                else
                {
                    crc32 = crc32_compute(p_buffer, buffer_len, &crc32);
                }

                memset(pkt, 0, mtu);

                req_offset += req_len; // 0+64=64,64+64=128,128+13=141
                bytes_sent = req_offset;

                bytes_remaining = fileSize - bytes_sent; // 141-64=77,141-128=13,141-141=0

                printf("[PC] bytes remaining: %d\r\n", bytes_remaining);
            }

            // 3. validate last sent obj
            if ( (bytes_sent == fileSize) /* file already sent, also when fileSize < max_size*/ || \
                 ((bytes_sent > 0) && (bytes_sent % max_size == 0)) )
            {
                nrfReceived = 0;
                nrf_crc32 = send_crc32_request(2000, &nrfReceived);

                printf("NRFCRC32: %u, PKTCRC32: %u\r\nTotal Received: %u bytes\r\n", nrf_crc32, crc32, nrfReceived);

                if (crc32 == nrf_crc32)
                {
                    last_crc32 = crc32;
                    last_req_offset = req_offset;

                    objValid = true;
                }
                else // resend last obj
                {
                    req_offset = last_req_offset;
                    bytes_sent = req_offset;
                    bytes_remaining = fileSize - bytes_sent;
                    crc32 = last_crc32;

                    printf("Resending obj from with offset: %u\r\n", req_offset);
                    break; // testing
                }
            }
        }

        // 4. execute obj
        if ( objValid )
        {
            objValid = false;

            send_execobj_request(2000);

            // obj is sent succesful, send remaining objs
            newobj = true;
        }
    } while (bytes_remaining != 0);

    if ( (bytes_sent == fileSize) && (bytes_remaining == 0) )
    {
        printf("File was sent successfully\r\n");
        status = true;
    }

    if (pkt != NULL)
    {
        free(pkt);
    }

    return status;
}



bool DFUSerial_init()
{
    esp_err_t err;

    uart_config_t uart_nrf_config = DEFAULT_NRF_UART_CONFIG;

    err = uart_driver_install(UART_NRF_PORT, NRF_RX_BUFFER_SIZE, NRF_TX_BUFFER_SIZE, 0, NULL, 0);
    
    if (err == ESP_OK)
    {
        err = uart_param_config(UART_NRF_PORT, &uart_nrf_config);
    }

    if (err == ESP_OK)
    {
        err = uart_set_pin(UART_NRF_PORT, UART_NRF_TXD, UART_NRF_RXD, UART_NRF_RTS, UART_NRF_CTS);
    }

    ESP_ERROR_CHECK(err);

    // before sending to NRF encode with SLIP protocol
    slipPkt.p_buffer = slipBuffer;
    slipPkt.current_index = 0;
    slipPkt.buffer_len = MAX_BUF_SIZE;
    slipPkt.state = SLIP_STATE_DECODING;

    return (err == ESP_OK);
}

bool DFUSerial_deinit()
{
    esp_err_t err = uart_driver_delete(UART_NRF_PORT);

    return (err == ESP_OK);
}

bool DFUSerial_ping(uint32_t timeout_ms)
{
    uint8_t ping_counter = 1;
    uint8_t ping_cmd[2] = {0};

    bool ping_response = false;

    uint32_t ping_timeout = get_millis() + timeout_ms;

    while ( get_millis() < ping_timeout )
    {
        ping_cmd[0] = NRF_DFU_OP_PING; // ping cmd
        ping_cmd[1] = ping_counter; // ping counter

        // send ping cmd
        dfu_send(ping_cmd, sizeof(ping_cmd));
        
        // read ping response
        slipBufferLen = 0;
        pSlipPkt = dfu_read(&slipBufferLen, DFU_RX_TIMEOUT_MS);

        if (slipBufferLen > 0)
        {
            ESP_LOG_BUFFER_HEX(TAG, pSlipPkt, slipBufferLen);

            if ( (pSlipPkt[0] == NRF_DFU_OP_RESPONSE) &&
                 (pSlipPkt[1] == ping_cmd[0]) &&
                 (pSlipPkt[2] == NRF_DFU_RES_CODE_SUCCESS) &&
                 (pSlipPkt[3] == ping_counter) )
            {
                ping_response = true;
                break;
            }
        }

        ping_counter++;
    }

    if (ping_response == false)
    {
        printf("Error: DFU target did respond to ping request.\r\n");
    }

    return ping_response;
}

bool DFUSerial_setPRN(uint16_t prn, uint32_t timeout_ms)
{
    bool status = false;

    uint8_t prn_cmd[3];

    uint32_t prn_timeout = get_millis() + timeout_ms;

    prn_cmd[0] = NRF_DFU_OP_RECEIPT_NOTIF_SET; // prn cmd
    prn_cmd[1] = prn & 0xFF;        // prn value LSB
    prn_cmd[2] = (prn >> 8) & 0xFF; // prn value MSB

    while ( get_millis() < prn_timeout )
    {
        printf("> Set PRN...\r\n");

        // set prn
        dfu_send(prn_cmd, sizeof(prn_cmd));

        // read response
        slipBufferLen = 0;
        pSlipPkt = dfu_read(&slipBufferLen, DFU_RX_TIMEOUT_MS);

        if (slipBufferLen > 0)
        {
            ESP_LOG_BUFFER_HEX(TAG, pSlipPkt, slipBufferLen);

            if ( (pSlipPkt[0] == NRF_DFU_OP_RESPONSE) && \
                 (pSlipPkt[1] == prn_cmd[0]) && \
                 (pSlipPkt[2] == NRF_DFU_RES_CODE_SUCCESS) )
            {
                status = true;
                break;
            }
        }
    }

    if (status == false)
    {
        printf("Error: DFU target did not respond with PRN acknowledegment.\r\n");
    }

    return status;
}

bool DFUSerial_getMTU(uint16_t *p_mtu, uint32_t timeout_ms)
{
    bool status = false;

    uint16_t mtu = 0;
    uint8_t mtu_cmd = NRF_DFU_OP_MTU_GET;

    uint32_t mtu_timeout = get_millis() + timeout_ms;

    while ( get_millis() < mtu_timeout )
    {
        printf("> Get MTU...\r\n");

        // get mtu
        dfu_send(&mtu_cmd, sizeof(mtu_cmd));

        // read response
        slipBufferLen = 0;
        pSlipPkt = dfu_read(&slipBufferLen, DFU_RX_TIMEOUT_MS);

        if (slipBufferLen > 0)
        {
            ESP_LOG_BUFFER_HEX(TAG, pSlipPkt, slipBufferLen);

            if ( (pSlipPkt[0] == NRF_DFU_OP_RESPONSE) && \
                 (pSlipPkt[1] == mtu_cmd) && \
                 (pSlipPkt[2] == NRF_DFU_RES_CODE_SUCCESS) )
            {
                mtu = 0;

                mtu |= pSlipPkt[3];      // LSB
                mtu |= pSlipPkt[4] << 8; // MSB

                *p_mtu = mtu/2;

                printf("MTU size: %u\r\n", *p_mtu);

                status = true;
                break;    
            }
        }
    }

    if (status == false)
    {
        printf("Error: DFU target did not respond with required MTU size.\r\n");
    }

    return status;
}

bool DFUSerial_selectObject(nrf_dfu_obj_type_t obj_type, uint32_t *max_size, uint32_t *cur_offset, uint32_t *cur_crc32, uint32_t timeout_ms)
{
    file_crc32 = 0; // new file, so clear it
    
    return select_obj_request(obj_type, max_size, cur_offset, cur_crc32, timeout_ms);
}

bool DFUSerial_createObject(nrf_dfu_obj_type_t obj_type, uint32_t obj_size, uint32_t timeout_ms)
{
    return create_obj_request(obj_type, obj_size, timeout_ms);
}

#if 0
bool DFUSerial_sendFile(const char *fileURL, nrf_dfu_obj_type_t objType, uint16_t targetMTU)
{
    bool status = false;
    int fileSize = 0;
    esp_err_t err;

    // initialize http client
    httpConfig.url = fileURL;
    httpClient = esp_http_client_init(&httpConfig);
    if ( httpClient == NULL )
    {
        printf("Error: Failed to initialize http client\r\n");
        return status;
    }

    // http connect
    err = esp_http_client_open(httpClient, 0);
    if ( err != ESP_OK )
    {
        printf("Error: http client failed to connect\r\n");
        return status;
    }

    // get file size
    fileSize = esp_http_client_fetch_headers(httpClient);
    if ( (fileSize < 0) || (fileSize == 0) )
    {
        printf("Error: Invalid filesize.\r\n");
        return status;
    }

    printf("Filesize: %d\r\n", fileSize);

    status = send_file_as_objs(fileSize, objType, targetMTU);

    esp_http_client_close(httpClient);
    esp_http_client_cleanup(httpClient);

    return status;
}
#endif

bool DFUSerial_sendObject(uint8_t *objData, uint16_t objLen, uint16_t nrf_mtu)
{
    uint32_t nrfReceived = 0;
    uint32_t nrf_crc32 = 0;

    uint16_t req_len = 0;
    uint16_t offset = 0;
    uint16_t bytes_remaining = objLen;
    uint16_t mtu_size = nrf_mtu - 1;

    bool status = false;

    uint8_t *p_pkt;

    uint8_t *pkt = (uint8_t *) malloc(sizeof(uint8_t) * nrf_mtu);

    if (pkt == NULL)
    {
        printf("Error: Failed to allocate memory for MTU.\r\n");
        return status; // false
    }

    p_pkt = pkt + 1;
    pkt[0] = NRF_DFU_OP_OBJECT_WRITE;

    do { 
        req_len =  (bytes_remaining > mtu_size) ? \
                    mtu_size : \
                    bytes_remaining;
        
        printf("req_len: %u, offset: %u, remaining: %u\r\n", req_len, offset, bytes_remaining);

        memcpy(p_pkt, objData+offset, req_len);
        ESP_LOG_BUFFER_HEX(TAG, pkt, req_len+1);

        dfu_send(pkt, req_len+1);

        offset = offset + req_len;
        bytes_remaining = objLen - offset;

        file_crc32 = (file_crc32 == 0) ? \
                    crc32_compute(p_pkt, req_len, NULL) : \
                    crc32_compute(p_pkt, req_len, &file_crc32);
    } while (bytes_remaining != 0);

    nrf_crc32 = send_crc32_request(DFU_CMD_TIMEOUT_MS, &nrfReceived);

    printf("CRC32: %u, NRFCRC32: %u, nrfReceived: %u\r\n", file_crc32, nrf_crc32, nrfReceived);

    status = (file_crc32 == nrf_crc32) ? \
                send_execobj_request(DFU_CMD_TIMEOUT_MS) : \
                false;

    if (pkt != NULL)
    {
        free(pkt);
    }

    return status;
}