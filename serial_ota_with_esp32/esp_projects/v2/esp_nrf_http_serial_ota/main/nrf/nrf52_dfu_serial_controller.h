#ifndef NRF52_DFU_SERIAL_CONTROLLER_H
#define NRF52_DFU_SERIAL_CONTROLLER_H

#include "driver/uart.h"
#include "driver/gpio.h"
#include "esp_http_client.h"

#define DEFAULT_NRF_UART_CONFIG {           \
    .baud_rate = 115200,                    \
    .data_bits = UART_DATA_8_BITS,          \
    .parity    = UART_PARITY_DISABLE,       \
    .stop_bits = UART_STOP_BITS_1,          \
    .flow_ctrl = UART_HW_FLOWCTRL_DISABLE,  \
    .source_clk = UART_SCLK_APB,            \
}

#define UART_NRF_PORT   UART_NUM_2
#define UART_NRF_TXD    GPIO_NUM_17
#define UART_NRF_RXD    GPIO_NUM_16

#define UART_NRF_RTS  (UART_PIN_NO_CHANGE)
#define UART_NRF_CTS  (UART_PIN_NO_CHANGE)

#define MAX_BUF_SIZE (256)

#define NRF_RX_TIMEOUT_MS   100 // UART RX timeout
#define DFU_RX_TIMEOUT_MS   300 // DFU RX timeout, retry 3 times
#define DFU_CMD_TIMEOUT_MS  300 // DFU TX timeout, retry 3 times

typedef enum {
    NRF_DFU_OBJ_TYPE_INVALID,
    NRF_DFU_OBJ_TYPE_COMMAND,
    NRF_DFU_OBJ_TYPE_DATA
} nrf_dfu_obj_type_t;

typedef enum {
  NRF_DFU_OP_PROTOCOL_VERSION = 0x00,
  NRF_DFU_OP_OBJECT_CREATE = 0x01,
  NRF_DFU_OP_RECEIPT_NOTIF_SET = 0x02,
  NRF_DFU_OP_CRC_GET = 0x03,
  NRF_DFU_OP_OBJECT_EXECUTE = 0x04,
  NRF_DFU_OP_OBJECT_SELECT = 0x06,
  NRF_DFU_OP_MTU_GET = 0x07,
  NRF_DFU_OP_OBJECT_WRITE = 0x08,
  NRF_DFU_OP_PING = 0x09,
  NRF_DFU_OP_HARDWARE_VERSION = 0x0A,
  NRF_DFU_OP_FIRMWARE_VERSION = 0x0B,
  NRF_DFU_OP_ABORT = 0x0C,
  NRF_DFU_OP_RESPONSE = 0x60,
  NRF_DFU_OP_INVALID = 0xFF
} nrf_dfu_op_t;

typedef enum {
  NRF_DFU_RES_CODE_INVALID = 0x00,
  NRF_DFU_RES_CODE_SUCCESS = 0x01,
  NRF_DFU_RES_CODE_OP_CODE_NOT_SUPPORTED = 0x02,
  NRF_DFU_RES_CODE_INVALID_PARAMETER = 0x03,
  NRF_DFU_RES_CODE_INSUFFICIENT_RESOURCES = 0x04,
  NRF_DFU_RES_CODE_INVALID_OBJECT = 0x05,
  NRF_DFU_RES_CODE_UNSUPPORTED_TYPE = 0x07,
  NRF_DFU_RES_CODE_OPERATION_NOT_PERMITTED = 0x08,
  NRF_DFU_RES_CODE_OPERATION_FAILED = 0x0A,
  NRF_DFU_RES_CODE_EXT_ERROR = 0x0B
} nrf_dfu_result_t;

bool DFUSerial_init();
bool DFUSerial_deinit();
bool DFUSerial_ping(uint32_t timeout);
bool DFUSerial_setPRN(uint16_t prn, uint32_t timeout_ms);
bool DFUSerial_getMTU(uint16_t *p_mtu, uint32_t timeout_ms);
bool DFUSerial_selectObject(nrf_dfu_obj_type_t obj_type, uint32_t *max_size, uint32_t *cur_offset, uint32_t *cur_crc32, uint32_t timeout_ms);
bool DFUSerial_createObject(nrf_dfu_obj_type_t obj_type, uint32_t obj_size, uint32_t timeout_ms);
// bool DFUSerial_sendFile(const char *fileURL, nrf_dfu_obj_type_t objType, uint16_t targetMTU);
bool DFUSerial_sendObject(uint8_t *objData, uint16_t objLen, uint16_t nrf_mtu);

#endif /* NRF52_DFU_SERIAL_CONTROLLER_H */