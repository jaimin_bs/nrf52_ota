#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include "esp_log.h"

#include "nrf52_dfu_serial_controller.h"
#include "nrf52_serial_ota.h"
#include "ota_http_client.h"
#include "ota_pcserial_client.h"

#define TAG "NRF52_SERIAL_OTA"

#define INIT_FILE_URL "https://gitlab.com/jaimin_bs/nrf52_ota/-/raw/main/http_binaries/blinky_v2/app_v2.dat" 
#define BIN_FILE_URL "https://gitlab.com/jaimin_bs/nrf52_ota/-/raw/main/http_binaries/blinky_v2/app_v2.bin"

otaState_et otaState = OTA_STATE_IDLE;
otaHost_et otaHost = OTA_HOST_INVALID;
otaState_et prevState = OTA_STATE_IDLE;

otaBuffer_st buffer;

bool otaStatus = false;
uint16_t targetMTU = 0;

uint32_t max_size = 0;
uint32_t cur_offset = 0;
uint32_t cur_crc32 = 0;

uint32_t curObjType;

static bool ota_dfu_init()
{
    printf("> Initializing OTA...\r\n");

    return DFUSerial_init();
}

static bool ota_dfu_connect_target()
{
    bool status = false;

    printf("> Connecting DFU target...\r\n");

    // try for 3 seconds, if pinged returns immediately
    status = DFUSerial_ping(DFU_CMD_TIMEOUT_MS);

    // set PRN to 0
    if ( status )
    {
        status = DFUSerial_setPRN(0, DFU_CMD_TIMEOUT_MS);
    }

    // get MTU for DFU
    if ( status )
    {
        status = DFUSerial_getMTU(&targetMTU, DFU_CMD_TIMEOUT_MS);
    }

    return status;
}

static bool ota_dfu_send_file(nrf_dfu_obj_type_t obj, const char *fileURL)
{
    bool status = false;

    if (obj == NRF_DFU_OBJ_TYPE_COMMAND)
    {
        printf("Sending init pkt file...\r\n");    
    }
    else if (obj == NRF_DFU_OBJ_TYPE_DATA)
    {
        printf("Sending firmware file...\r\n");
    }

    status = DFUSerial_selectObject(obj, &max_size, &cur_offset, &cur_crc32, DFU_CMD_TIMEOUT_MS);

    if (status)
    {
        // set obj for ota_run_dfu()
        curObjType = obj;

        buffer.data = (uint8_t *) malloc(sizeof(uint8_t) * max_size);
        buffer.size = max_size;

        if (buffer.data == NULL)
        {
            printf("Error: Failed to allocate memory for http buffer.\r\n");
            status = false;
        }
    }

    // starts http file download
    if (status)
    {
        if (otaHost == OTA_HOST_HTTP)
        {
            http_ota_init(fileURL, &buffer);
            http_ota_start();
        }
        else if (otaHost == OTA_HOST_PCSERIAL)
        {
            pcserial_ota_init(curObjType, &buffer);
            pcserial_ota_start();
        }
    }

    return status;
}

static otaState_et ota_run_dfu()
{
    static otaState_et nextState;
    static bool send_obj;
    static bool obj_sent;

    printf("received: %d, eof: %d\r\n", buffer.dataReceivedLen, buffer.eof);

    // data available, send to nrf
    if (buffer.dataReceivedLen > 0)
    {
        ESP_LOG_BUFFER_HEX(TAG, buffer.data, buffer.dataReceivedLen);

        send_obj = DFUSerial_createObject(curObjType, buffer.dataReceivedLen, DFU_CMD_TIMEOUT_MS);

        if (send_obj)
        {
            do {
                obj_sent = DFUSerial_sendObject(buffer.data, buffer.dataReceivedLen, targetMTU);
            } while (obj_sent == false);
        }
    }

    nextState = buffer.eof ? OTA_STATE_DFU_FILE_SENT : \
                             OTA_STATE_DFU_IN_PROGRESS;

    return nextState;
}

static void ota_free_buffer()
{
    if (buffer.data != NULL)
    {
        free(buffer.data);
    }
}

static void ota_clean_up()
{
    DFUSerial_deinit();
}

void ota_sync()
{
    // all of the ota state tranisitions are handled here itself
    switch (otaState)
    {
        // do nothing
        case OTA_STATE_IDLE:
            break; 
        
        // initialize resources: uart
        case OTA_STATE_DFU_INIT:
            printf("OTASTATE: INIT\r\n");
            otaState = ota_dfu_init() ? OTA_STATE_DFU_CONNECT : OTA_STATE_END;
            break;
        
        // connect to dfu target
        case OTA_STATE_DFU_CONNECT:
            printf("OTASTATE: CONNECT\r\n");
            otaState = ota_dfu_connect_target() ? \
                        OTA_STATE_DFU_SEND_INIT_FILE : \
                        OTA_STATE_END;
            break;
        
        case OTA_STATE_DFU_SEND_INIT_FILE:
            printf("OTASTATE: SEND INIT FILE\r\n");
            prevState = otaState;
            otaState = ota_dfu_send_file(NRF_DFU_OBJ_TYPE_COMMAND, INIT_FILE_URL) ? \
                        OTA_STATE_DFU_IN_PROGRESS : \
                        OTA_STATE_END;
            break;
        
        case OTA_STATE_DFU_SEND_FIRMWARE_FILE:
            printf("OTASTATE: SEND FIRMWARE FILE\r\n");
            prevState = otaState;
            otaState = ota_dfu_send_file(NRF_DFU_OBJ_TYPE_DATA, BIN_FILE_URL) ? \
                        OTA_STATE_DFU_IN_PROGRESS : \
                        OTA_STATE_END;
            break;

        // start ota update procedure
        // file is fragmented into, send-obj -> exec-obj
        // each obj is further fragmented into packets of MTU
        case OTA_STATE_DFU_IN_PROGRESS:
            printf("OTASTATE: DFU IN PROGRESS\r\n");
            otaState = ota_run_dfu(); // returns OTA_STATE_DFU_IN_PROGRESS or
                                      // OTA_STATE_DFU_FILE_SENT
            break;
        
        // after init file has been sent, send firmware file
        // after firmware file has been sent, otaStatus = true
        case OTA_STATE_DFU_FILE_SENT:
            printf("OTASTATE: FILE SENT\r\n");
            otaStatus = (prevState == OTA_STATE_DFU_SEND_FIRMWARE_FILE);
            otaState = (prevState == OTA_STATE_DFU_SEND_INIT_FILE) ? \
                           OTA_STATE_DFU_SEND_FIRMWARE_FILE : \
                           OTA_STATE_END;
            ota_free_buffer();
            break;
        
        // end, check ota status & free buffer
        case OTA_STATE_END:
            printf("OTA status: %s\r\n", otaStatus ? "success" : "failed");
            ota_clean_up();
            prevState = OTA_STATE_IDLE;
            otaState = OTA_STATE_IDLE;
            break;
    }
}

void ota_start(otaHost_et host)
{
    otaState = OTA_STATE_DFU_INIT;
    otaHost = host;
}