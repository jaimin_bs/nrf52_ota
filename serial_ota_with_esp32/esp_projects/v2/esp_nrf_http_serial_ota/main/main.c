/* ESP HTTP Client Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/

#include <string.h>
#include <stdlib.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_log.h"
#include "esp_system.h"
#include "nvs_flash.h"
#include "esp_event.h"
#include "esp_netif.h"
#include "protocol_examples_common.h"

#include "driver/gpio.h"

#include "nrf52_serial_ota.h"
#include "ota_http_client.h"
#include "ota_pcserial_client.h"

static const char *TAG = "MAIN";

volatile bool btn = false;

static void IRAM_ATTR gpio_isr_handler(void* arg)
{
    btn = true; 
}

void system_task(void *pvParameters)
{
    while(1)
    {
        ota_sync();
        http_ota_sync();
        pcserial_ota_sync();
        
        vTaskDelay(pdMS_TO_TICKS(20));
    }
}

void app_main(void)
{
    gpio_config_t io_conf;

    io_conf.pin_bit_mask = (1<<GPIO_NUM_0);
    io_conf.mode = GPIO_MODE_INPUT;
    io_conf.pull_up_en = 1;
    io_conf.intr_type = GPIO_INTR_NEGEDGE;
    gpio_config(&io_conf);
    gpio_install_isr_service(0);
    gpio_isr_handler_add(GPIO_NUM_0, gpio_isr_handler, NULL);

    esp_err_t ret = nvs_flash_init();
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
      ESP_ERROR_CHECK(nvs_flash_erase());
      ret = nvs_flash_init();
    }
    ESP_ERROR_CHECK(ret);
    ESP_ERROR_CHECK(esp_netif_init());
    ESP_ERROR_CHECK(esp_event_loop_create_default());

    /* This helper function configures Wi-Fi or Ethernet, as selected in menuconfig.
     * Read "Establishing Wi-Fi or Ethernet Connection" section in
     * examples/protocols/README.md for more information about this function.
     */
    ESP_ERROR_CHECK(example_connect());
    ESP_LOGI(TAG, "Connected to AP, begin http example");

    xTaskCreate(&system_task, "system_task", 8192, NULL, 5, NULL);

    while(1)
    {
        if (btn)
        {
            btn = false;

            printf("Pressed\r\n");
                
            // ota_start(OTA_HOST_HTTP);
            ota_start(OTA_HOST_PCSERIAL);
        }

        vTaskDelay(pdMS_TO_TICKS(100));
    }
}
